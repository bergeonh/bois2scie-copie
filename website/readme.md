# Usage
Il y a 2 branches principales sur ce site avec 2 configurations necéssaire au fonctionnement:
### Master : 
Pour utiliser ce site en **local** il suffit d'utiliser [PHP](https://www.php.net/)
<br>
Dans un terminal, exécuter la commande suivante :
```bash
php -S localhost:8080
```
#### Curl :
le site possède plusieurs fonction nécessitant curl.

Pour Linux:
```bash
# apt-get install curl
```
Source : https://www.tecmint.com/install-curl-in-linux/


Pour windows:
1.télécharger cacert.pem au lien:
https://curl.se/ca/cacert.pem

2.il faut modifier la ligne "curl.cainfo" dans le fichier php.ini avec le chemin menant au fichier cacert.pem (point 1).

#### Sqlite:

Pour windows
Pour faire focntionner la base de données en localhost, Modifier dans la partie sqlite3 de php.ini:

```txt
sqlite3.extension_dir = "ext"
extension = sqlite3
extension = pdo_sqlite
```

#### Serveur OSRM:
Source: https://reckoningrisk.com/OSRM-server/ <br>
A utiliser avec cette carte : http://download.geofabrik.de/europe/france/rhone-alpes-latest.osm.pbf <br>
Ainsi s'il y a une commande, remplacer "delaware" par "rhone-alpes" <br>

Une fois setup, le calcul de destination peut etre fait via la page "confirmation Panier"

### sansCalculDistance :
Cette partie fonctionne via les serveurs de l'iut pour utiliser le serveur mail de l'iut, elle est quasiment fonctionel en local mais on ne pourra pas envoyer de mail. Cepandant on n'aura pas de calcul de distance car l'implémentation demande un accès root et un espace de stockage trop important.
Il suffira donc de déposer tout le projet dans un dossier public_html puis exécuter la commande :
```bash
setup-public-html
```
puis aller sur le lien suivant : 
```bash
http://www-etu-info.iut2.upmf-grenoble.fr/~nomDeLogin/
```
**les différents test comme DAO.test.php sont fonctionnels uniquement dans cette partie**
# Droits d'usage

Utilisataion de l'osrm serveur:

Logan, T. M., T. G. Williams, A. J. Nisbet, K. D. Liberman, C. T. Zuo, and S. D. Guikema. 2017. “Evaluating Urban Accessibility: Leveraging Open-Source Data and Analytics to Overcome Existing Limitations.” Environment and Planning B: Urban Analytics and City Science, November.
https://reckoningrisk.com/OSRM-server/

Toutes les données sont sous licence ODbl
Avec la contribution de [nominatim](https://nominatim.org/)

Avec la contribution de [© les contributeurs d’OpenStreetMap](https://www.openstreetmap.org/#map=19/45.19209/5.71736)
