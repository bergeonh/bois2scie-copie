--Insertions d'entreprises
select 'Insertions d''entreprises' AS '';
INSERT INTO Entreprise(nom, nomContact, adresse, siret, tva_intra, codeAPE)
VALUES('arcopharma', 'didier', '12ruedeschamps', 110142, 1215412, 'jesuisuncodeAPE');

INSERT INTO Entreprise(nom, adresse, siret, tva_intra, codeAPE, coord_bank)
VALUES('nicolasEntreprise', 'LePaysDesBisous', 110143, 1215412, 'jesuisunautrecodeAPE','etmoidescoordbankaire');


--Insertions d'utilisateurs
select 'Insertions d''utilisateurs' AS '';
INSERT INTO Utilisateur(email,mdp,prenom,nom,adresse,date_connexion, date_destruction)
VALUES('hippolyte.bergeon@gmail.com','$argon2i$v=19$m=2048,t=4,p=3$UUR1YXphUVk0WDBHTjZxVg$FrxwcNGXwD/EgFku4TtSOOexIPugfi888/qO8Jsktu8','Hippolyte','Bergeon','ici','','');
--mdp = azerty

INSERT INTO Utilisateur(email,mdp,prenom,nom,adresse,statut, id_entreprise)
VALUES('sacha.blanco@yahoo.fr','$argon2i$v=19$m=2048,t=4,p=3$RUF2V2JrMmxTbXpONmxGZw$qu46kCauG37J7hu5nnGxEA2lfCr1A7fceebvWjISMQg','Sacha','blanco','Acotedeliut','entreprise', 2);
--mdp = hahaIlEstSurYahoo

INSERT INTO Utilisateur(email,mdp,prenom,nom,adresse,statut, id_entreprise)
VALUES('lukas.loyo@yahoo.fr','$argon2i$v=19$m=2048,t=4,p=3$U2JNdmxSUE12RC5oc2V4aA$bVCD/gHBDAP+GTulTaPtK9CcoFQ+loMxCt4LaVO84pg','Loyo','lukas','loindeliut', 'entreprise', 1);
--mdp = mdp

--Doit lever le trigger ValidationEmail
INSERT INTO Utilisateur(email,mdp,prenom,nom,adresse,date_connexion, date_destruction)
VALUES('mauvaisEmail@fr','$argon2i$v=19$m=2048,t=4,p=3$U2JNdmxSUE12RC5oc2V4aA$bVCD/gHBDAP+GTulTaPtK9CcoFQ+loMxCt4LaVO84pg','faux','compte','placeholder','','');
--mdp = mdp

--Insertions de lots bruts
select 'Insertions de lots bruts' AS '';
INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A101121Mlz15nic','10/11/2021',2,'Mélèze','15/11/2021','15.2','950.00','Un bon bois tah lépoque');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A121121Mlz15nic','12/11/2021',2,'Mélèze','15/10/2020','15.2','950.00','Un bon bois tah lépoque');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A131121Mlz15nic','13/11/2021',2,'Mélèze','14/10/2020','15.2','950.00','Un bon bois tah lépoque');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A111121Mlz15nic','11/11/2021',2,'Mélèze','15/11/2020','15.2','950.00','Un bon bois tah lépoque');


INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A101121Cht15arc','10/11/2021',1,'Châtaigner','15/11/2021','15.2','950.00','Un bois de broke');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A101121Chn15arc','10/11/2021',1,'Chêne','25/12/2021','15.2','950.00','Un bois de broke');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A211221Htr15arc','21/12/2021',1,'Hêtre','25/12/2021','15.2','950.00','Un bois de broke');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A101121Dou15arc','10/11/2021',1,'Douglas','25/12/2021','15.2','950.00','Un bois de broke');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A211221pic15arc','21/12/2021',1,'Épicéa','25/12/2021','15.2','950.00','Un bois de broke');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A211221Pin15arc','21/12/2021',1,'Pin','25/12/2021','15.2','950.00','Un bois de broke');

INSERT INTO LotBrut(ref_lot,date_achat,id_entreprise,essence,date_prelevement,quantite,prixAchat,description)
VALUES('A211221Sap15arc','21/12/2021',1,'Sapin','25/12/2021','15.2','950.00','Un bois de broke');


--Insertions de bois transformés
select 'Insertions bois transformés' AS '';
INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A101121Mlz15nic',163.00,290,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A121121Mlz15nic',120.00,70,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A101121Chn15arc',50.00,134,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A101121Dou15arc',102.00,120.00,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A211221pic15arc',102.00,120.00,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A211221Pin15arc',102.00,120.00,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A211221Sap15arc',102.00,120.00,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A101121Cht15arc',102.00,120.00,1.52);

INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
VALUES('A211221Htr15arc',163.00,290,1.52);



--Insertions de pièces
select 'Insertions de pièces' AS '';
INSERT INTO Piece(ref_bois,longueur,largeur,hauteur,typePiece, prix)
VALUES(1, 1 ,0.5,0.2,'type1', 12.7);

INSERT INTO Piece(ref_bois,longueur,largeur,hauteur,typePiece, prix)
VALUES(2, 3 ,0.5,0.2,'type2', 51.23);

--Insertions de commandes
select 'Insertions de commandes' AS '';


INSERT INTO Commande(id_utilisateur)
VALUES(1);

INSERT INTO Commande(id_utilisateur,etat)
VALUES(1, 'arrive');

INSERT INTO Commande(id_utilisateur,etat)
VALUES(1,'expedie');

INSERT INTO Commande(id_utilisateur,etat)
VALUES(1,'annule');

INSERT INTO Commande(id_utilisateur)
VALUES(3);

--Insertions de détails
select 'Insertions de détails' AS '';
INSERT INTO Detail(ref_piece,id_commande,nbPiece)
VALUES(1,1,5);

INSERT INTO Detail(ref_piece,id_commande,nbPiece)
VALUES(2,5,1);

--Insertions de réduction
select 'Insertion de réductions' AS '';
INSERT INTO Reduction(nom_reduc,value)
VALUES('demarage',0.5);

INSERT INTO Reduction(nom_reduc,value)
VALUES('uneReduction',0.25);


--Insertions de descriptions de bois
INSERT INTO InfosEssence(description, essence)
VALUES("
<h3>Présentation</h3> <br>

<p>
Ce bois est très apprécié pour la qualité de son grain fin ainsi que pour sa teinte brun rosâtre souvent veinée de brun rougeâtre. 
</p>

<h3>Durabilité</h3>

<p>
Au-delà de son esthétique, le bois de mélèze possède des caractéristiques intéressantes, que ce soit d’un point de vue de sa durabilité naturelle 
ou de ses propriétés physiques et mécaniques.
Sa dureté correcte l’autorise à être utilisé en revêtement de sol pour sa bonne résistance au poinçonnement. 
Il peut être utilisé en extérieur en classe 3 (hors contact du sol) sans traitement car il est suffisamment durable et résistant aux agents de dégradation existants dans cette classe d’emploi.
</p>

<h3>Applications</h3>

<p>
Il s’agit d’une essence qui est utilisée dans de nombreux domaines et notamment pour des lames de bardage ou terrasse en extérieur et de parquet en intérieur.
Dans les Alpes, cette essence est traditionnellement utilisée pour les bardeaux de toiture.
Utilisé sans protection, le mélèze est connu pour prendre une patine grise argentée du plus bel effet.
</p>", 'Mélèze');

INSERT INTO InfosEssence(description, essence)
VALUES("

<h3>Présentation</h3> <br>

<p>
    Le Châtaignier est reconnu comme l'une des plus belles et plus durables essences de nos forêts françaises. C’est un bois feuillu aux nombreuses qualités.
</p>


<h3>Durabilité</h3>

<p>
    Naturellement durable : ne nécessite aucune préservation
    Résistant : à la plupart des insectes xylophages
</p>


<h3>Applications</h3>

<p>

</p>
", 'Châtaigner');

INSERT INTO InfosEssence(description, essence)
VALUES("
<h3>Présentation</h3> <br>

<p>
Le Chêne est une essence locale naturellement durable utilisée depuis toujours pour des réalisations destinées à résister dans le temps. <br>
C’est une essence de bois feuillu à croissance lente, qui présente de nombreuses qualités.
Il présente une noblesse et esthétique avec ses variations de couleurs uniques. <br>
Le chêne a un cycle d’exploitation sur le temps long (on parle de révolution) de 120 à 250 ans. 
Car ce n’est qu’après 150 ans que le tronc présente un diamètre de 60 à 80 cm permettant dès lors sa transformation. 
Cette essence présente un grain moyen, un fil droit, et une bonne stabilité dimensionnelle. Elle présente une belle teinte brun clair à brun jaune. 
C’est une essence qui présente un bon rendement matière, qui s’usine assez aisément, en donnant un état de surface parfaitement lisse et soyeux au toucher. 
Tout comme le Châtaignier, le Chêne contient un tanin, qui se révèle au contact de l'eau, pouvant créer des colorations ou coulures disgracieuses. 
</p>


<h3>Durabilité</h3>

<p>
    Durabilité naturelle, il ne nécessite aucune préservation. <br>
    Résistant et naturellement durable, sans entretien particulier.
    Le bois de Chêne présente un duramen durable (classe de durabilité naturelle II).
    Naturellement durable (classe d'emploi 4 en extérieur si purgé d'aubier).
</p>


<h3>Applications</h3>

<p>
    En utilisation extérieure, il est donc conseillé de le protéger avec un saturateur bloqueur de tanin si l'on souhaite conserver une teinte homog
    Pour les produits extérieurs : bardage, terrasse, clôtures. C’est donc une essence locale très résistante à l'extérieur sans traitement de préservation.
    Le Chêne est un matériau idéal pour la construction en bois pérenne. tonnellerie pour l’élevage des vins ou spiritueux, en placage ou ébénisterie, en menuiserie pour la fabrication de parquets et de meubles. 

</p>
", 'Chêne');
INSERT INTO InfosEssence(description, essence)
VALUES("
<h3>Présentation</h3> <br>

<p>
    Séchage facile avec une tendance à se fissurer.
    Sciage sans difficulté.
    Collage sans difficulté.
    Ses fibres favorisent un usinage avec une très bonne finition.
    Très adapté au déroulage.
    Facile à travailler, le hêtre offre des possibilités très variées.
    Il se teinte aisément avec une très belle qualité de surface.
    Se cintre bien après étuvage.
</p>


<h3>Durabilité</h3>

<p>
 Non durable mais facilement imprégnable.
 Classe d'mploi: classe 2 - à l'intérieur ou sous abri (risque d'humidification)
</p>


<h3>Applications</h3>

<p>
    Agencement intérieur
    Parquets, escaliers, objets du quotidien (jouets, plateaux, ustensiles de cuisine, décoration…)
    Pièces de mobilier courbé (coques de chaises, bureaux, chevets…)
    Aménagement extérieur (terrasses, bardages) après traitement THT
</p>
", 'Hêtre');

INSERT INTO InfosEssence(description, essence)
VALUES("
<h3>Présentation</h3> <br>

<p>
Le bois de douglas a un duramen rose-saumon à brun rougeâtre, un aubier distinct plus pâle. 
Le fil est droit. Les cernes larges (croissance rapide) et une texture forte (grande proportion de bois final). 
Au sein d’un même cerne annuel, il existe une hétérogénéité de couleur et de structure, due au diamètre différent des vaisseaux du bois, entre le bois initial (bois de Printemps), 
le bois intermédiaire et le bois final (bois d’été).
C’est un bois mi-dur, mi-lourd, assez peu nerveux dont la densité varie entre 0,45 et 0,60.
Acceptant des conditions de climat plutôt froides et arrosées, le douglas, arbre à croissance rapide et longévité exceptionnelle.
Le douglas est un arbre a fût très droit doté d’une croissance spectaculaire pouvant atteindre, dans son pays d’origine, les 100 m et d’une longévité pouvant dépasser les 1000 ans.
</p>


<h3>Durabilité</h3>

<p>
Durabilité naturelle Classe II.
Class d'emploi: classe 3.2 hors aubier
</p>


<h3>Applications</h3>

<p>
Présentant des qualités de durabilité naturelle et de résistance mécanique, le douglas est couramment utilisé en construction (charpente/ossature).
Le douglas est un des bois d’œuvre qui permet des utilisations aussi variées que la charpente (traditionnelle et fermette), 
la construction navale, la menuiserie d’intérieure et d’extérieure, le placage, le plancher, le parquet, le poteau, 
le panneau contreplaqué ou lamellé-collé, le plafond, le lambris…
</p>

", 'Douglas');

INSERT INTO InfosEssence(description, essence)
VALUES("

<h3>Présentation</h3> <br>
<p>
    Le bois d’épicéa est très clair, blanchâtre à crème. Chez les épicéas de montagne, il est lustré et même parfois nacré. 
    Les cernes sont apparents, circulaires et de largeurs homogènes. L’épicéa provenant de régions froides présente des cernes étroits et réguliers. 
    Présence fréquente de poches de résine, qui sont souvent écartées en production pour les produits nobles tels que lambris et bardages.
    L’Epicéa est très largement employé dans la construction. C’est une essence de bois qui présente de nombreuses qualités
    Noblesse et esthétique avec un fil droit et une teinte crème qui accueille bien les finitions.
    Esthétique – Coloris naturel, et chaleureux pour les intérieurs.
    Homogène - Petits nœuds sains et adhérents.
</p>


<h3>Durabilité</h3>

<p>
    Accepte les traitements et finitions, il devient alors résistant et durable.
    Respectueux de l’environnement, 100% écologique.
    Il est conseillé d’appliquer une finition pour prolonger sa durée de vie, et son esthétique. La finition donne de bons résultats pour autant que le bois soit suffisamment sec.
</p>


<h3>Applications</h3>

<p>
L’épicéa n’est pas durable naturellement pour un usage extérieur et le bois se laisse difficilement imprégner. 
Mais comme l’eau pénètre donc moins vite dans la structure du bois, cela retarde aussi les risques d’altérations fongiques. 
D'une bonne stabilité dimensionnelle, il est durable jusqu'à 50 ans s'il a été préservé en classe 3.1, et s'il est recouvert d'une finition entretenue. 
L’usage en extérieur réclame une préservation par traitement en autoclave sous vide ou pression, pour lui conférer une durabilité appropriée.
</p>

", 'Épicéa');

INSERT INTO InfosEssence(description, essence)
VALUES("
<h3>Présentation</h3> <br>

<p>
    Le Pin Sylvestre est l’essence la plus largement employée dans la construction. C’est une essence de bois qui présente de nombreuses qualités.
    Noblesse et esthétique avec un fil droit et une teinte crème qui accueille bien les finitions.
    Esthétique – Coloris naturel, et chaleureux pour les intérieurs.
    Homogène - Petits nœuds sains et adhérents.
    Accepte les traitements et finitions, il devient alors résistant et durable.
    Respectueux de l’environnement, 100% écologique.
</p>


<h3>Durabilité</h3>

<p>
Il est conseillé d’appliquer une finition pour prolonger sa durée de vie, et son esthétique. La finition donne de bons résultats pour autant que le bois soit suffisamment sec.

</p>


<h3>Applications</h3>

<p>

</p>

Durabilité conférée pour de multiples usages.
", 'Pin');

INSERT INTO InfosEssence(description, essence)
VALUES("

<h3>Présentation</h3> <br>

<p>
Le bois de sapin est blanc crème, parfois avec quelques colorations rosées. 
Les cernes sont bien marquées et le fil est droit. Le grain est fin à moyen selon la vitesse de croissance. Le duramen n’est pas distinct de l’aubier.

</p>


<h3>Durabilité</h3>

<p>

</p>


<h3>Applications</h3>

<p>
    Charpente
    Ossature
    Poteaux
    Lamellé-collé, contre collé
    Menuiserie intérieure
</p>
", 'Sapin');
