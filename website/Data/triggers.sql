--Un trigger pour empêcher l'ajout d'un email invalide
CREATE TRIGGER ValidationEmail 
   BEFORE INSERT ON Utilisateur
BEGIN
   SELECT
      CASE
	WHEN NEW.email NOT LIKE '%_@__%.__%' THEN
   	  RAISE (ABORT,'Email invalide')
       END;
END;