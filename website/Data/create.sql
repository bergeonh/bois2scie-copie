CREATE TABLE Entreprise (
  id_entreprise INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  nom TEXT NOT NULL,
  nomContact TEXT DEFAULT 'void',
  tel_fixe TEXT DEFAULT 'void',
  tel_port TEXT DEFAULT 'void',
  adresse TEXT NOT NULL,
  siret INTEGER NOT NULL UNIQUE,
  tva_intra INTEGER NOT NULL,
  codeAPE TEXT NOT NULL,
  coord_bank TEXT DEFAULT 'void',
  conditionGen TEXT DEFAULT 'void',
  commentaires TEXT DEFAULT 'void'
);

CREATE TABLE Utilisateur (
  id_utilisateur INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  id_entreprise INTEGER DEFAULT 0,
  email TEXT NOT NULL UNIQUE,
  mdp TEXT NOT NULL,
  prenom TEXT NOT NULL,
  nom TEXT NOT NULL,
  adresse TEXT DEFAULT 'void',
  statut TEXT CHECK(statut IN ('particulier', 'entreprise'))  NOT NULL DEFAULT 'particulier',
  date_connexion TEXT DEFAULT 'void',
  date_destruction TEXT DEFAULT 'void',
  FOREIGN KEY(id_entreprise) REFERENCES Entreprise(id_entreprise)
);

CREATE TABLE LotBrut(
  ref_lot TEXT PRIMARY KEY NOT NULL,
  id_entreprise INTEGER NOT NULL,
  date_achat TEXT NOT NULL,
  essence TEXT NOT NULL,
  date_prelevement TEXT NOT NULL,
  quantite REAL NOT NULL,
  prixAchat REAL NOT NULL,
  description TEXT DEFAULT 'void',
  FOREIGN KEY(id_entreprise) REFERENCES Entreprise(id_entreprise)
);

CREATE TABLE BoisTransforme(
  ref_bois INTEGER PRIMARY KEY AUTOINCREMENT,
  ref_lot TEXT NOT NULL,
  prixCube REAL NOT NULL,
  quantite REAL NOT NULL,
  perte REAL NOT NULL,
  FOREIGN KEY(ref_lot) REFERENCES LotBrut (ref_lot)
);

CREATE TABLE Commande(
  id_commande INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  id_utilisateur INTEGER NOT NULL,
  depart TEXT DEFAULT '',
  arrivee TEXT DEFAULT '',
  etat TEXT CHECK(etat IN ('panier', 'valide','approuve', 'paye', 'expedie', 'arrive','annule'))  NOT NULL DEFAULT 'panier',
  date TEXT DEFAULT '01/01/00',
  FOREIGN KEY(id_utilisateur) REFERENCES Utilisateur (id_utilisateur)
);

CREATE TABLE Piece(
  ref_piece INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  ref_bois INTEGER NOT NULL,
  longueur REAL NOT NULL,
  largeur REAL NOT NULL,
  hauteur REAL NOT NULL,
  prix REAL DEFAULT -1.0,
  typePiece TEXT CHECK(typePiece IN ('type1', 'type2', 'type3', 'type4')) NOT NULL,
  FOREIGN KEY (ref_bois) REFERENCES BoisTransforme (ref_bois)
);

CREATE TABLE Detail(
  ref_piece INTEGER NOT NULL,
  id_commande INTEGER NOT NULL,
  nbPiece INTEGER NOT NULL,
  PRIMARY KEY (ref_piece,id_commande),
  FOREIGN KEY (ref_piece) REFERENCES Piece (ref_piece),
  FOREIGN KEY (id_commande) REFERENCES Commande (id_commande)
);

CREATE TABLE Reduction(
  nom_reduc TEXT PRIMARY KEY NOT NULL,
  value float CHECK(value <= 1) DEFAULT 1 
);

CREATE TABLE InfosEssence(
  description TEXT NOT NULL, 
  essence TEXT NOT NULL,
  FOREIGN KEY (essence) REFERENCES LotBrut (essence)
);