<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="icon" href="../Resources/bois2scie/fav.ico" />
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/shipment.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <title>Bois 2 Scies - Livraison</title>
</head>

<body>
    <?php include('SubView/header.subview.php')?>

    // Zone pour les coordonnées postale
    <div class="adresse-wrapper">
        <h2>Adresse de livraison</h2>
        <div class="adresse-container">
            <div class="prenom input">
                <input type="text" placeholder="Prénom">
            </div>

            <div class="nom input">
                <input type="text" placeholder="Nom">
            </div>

            <div class="entreprise input">
                <input type="text" placeholder="Entreprise (optionnel)">
            </div>

            <div class="adresse input">
                <input type="text" placeholder="Adresse">
            </div>

            <div class="apt input">
                <input type="text" placeholder="Apt, suite, etc (optionnel">
            </div>

            <div class="ville input">
                <input type="text" placeholder="Ville">
            </div>

            <div class="codepostale input">
                <input type="text" placeholder="Code postale">
            </div>

            <div class="tel input">
                <input type="text" placeholder="Téléphone">
            </div>
        </div>
    </div>

    // Zone pour les coordonnées bancaire
    <div class="paiement-wrapper">
        <h2>Paiement</h2>
        <h3>Carte de crédit</h3>
        <div class="paiement-container">
            <div class="numcarte">
                <input type="text" placeholder="Numéro de carte">
            </div>

            <div class="fullname">
                <input type="text" placeholder="Nom complet">
            </div>

            <div class="expdate">
                <input type="text" placeholder="Date d'expiration (MM/AA)">
            </div>

            <div class="cvc">
                <input type="text" placeholder="CVC">
            </div>

        </div>
    </div>

    <div class="checkout">
        <h3 class="prix">10.00€</h3>
        <input type="submit" class="button" value="Procéder au paiement">
    </div>

    <?php include('SubView/footer.subview.php')?>
</body>

</html>