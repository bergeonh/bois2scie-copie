<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/choose.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <title>Bois 2 Scies</title>
</head>

<body>

<?php include('SubView/header.subview.php')?>

    <main>
       <img class="bg_image" src="../Resources/assets/choisir_bg_crop.jpg" alt="main">
    </main>

    <!-- TODO -->
    <article>
        <h3>METTEZ LA BONNE ESSENCE AU BON ENDROIT</h3>
        <p>La durée de vie des essences change en fonction de leur utilisation.
            Elle dépend de son exposition aux agents climatiques et à sa durabilité naturelle.
            Cette durabilité naturelle varie en fonction des essences. Si la durée de vie naturelle 
            n'est pas suffisante, il faut alors appliquer un traitement de préservation pour prolonger cette durée.
        </p>
    </article>

    <section>    
        <article>
            <!-- TODO: schema of Emplois -->
            <h3>COMMENT SAVOIR SI J'AI BESOIN DE TRAITER MON BOIS ?</h3>
            <p>
                On distingue 5 classes du bois, définies par la norme NF EN 335-1 à 3
                A noter que ces classes ne concernent que le duramen (ce que nous vendons) et non l'aubier.

            <br><br><br>
            <b> Classe 1 : </b>Cette classe concerne les bois secs utilisé à l’intérieur avec un taux d’humidité inférieur à 20 %. Ces bois sont utilisés pour les menuiseries intérieures à l’abri de l’humidité.
            <br><br>
            <b> Classe 2 : </b>La classe 2 définis les bois secs pouvant être occasionnellement en contact avec un taux d’humidité supérieur à 20%, comme les ossatures et charpentes.
            <br><br>
            <b> Classe 3 : </b>Cette classe regroupe les bois pouvant être en contact fréquent avec l’humidité, même au-delà de 20%. Ce type de bois est très utilisé pour le bardage.
            <br><br>
            <b> Classe 4 : </b>Les bois de la classe 4, sont des essences qui ne craignent pas un contact avec l’eau douce, ces bois sont stabilisés et imputrescibles. Certains le deviennent à l’aide d’un traitement autoclave ou lorsqu’ils sont thermochauffés. Les bois exotiques quant à eux sont naturellement de classe 4 naturellement.
            <br><br>
            <b> Classe 5 : </b>La classe 5 regroupe les essences susceptibles d’être en contact permanent avec l’eau salée, ces bois extrêmement durables.
            </p>
            <br><br><br>
            <p>
                Bois2Scies ne s'occupe pas de la traite du bois et ne vend que du bois non traité.
                En fonction de la classe d'emploi que nécessite votre ouvrage, vous devrez choisir la bonne essence et possiblement un traitement.
            
                <br><br><br>
                Sans traitements, les classes d'emploi des essences locales sont les suivantes:
                <br> <br>
                <b> Chêne: </b>       classes 1 à 4
                <br>
                <b> Châtaignier: </b> classes 1 à 3.2
                <br>
                <b> Robinier: </b>    classes 1 à 4
                <br>
                <b> Mélèze: </b>      classes 1 à 3.2
                <br>
                <b> Douglas: </b>    classes 1 à 3.2
                <br>

                <br><br><br>
                Avec traitements, les classes d'emploi des essences locales sont les suivantes:
                <br> <br>
                <b> Hêtre: </b>       classes 1 à 4
                <br>
                <b> Pin: </b> classes 1 à 4
                <br>
                <b> Sapin: </b>    classes 1 à 3.2
                <br>
                <b> Epicéa: </b>      classes 1 à 3.1
                <br>
            </p>
        </article>
        

        <article>
            <h3>BESOIN D'AIDE POUR LE VOCABULAIRE ? <br> CONSULTEZ LE GLOSSAIRE:</h3>
            <br> <br> <br>

            <h4>Essence</h4>
            <br>
            <p>
            Dans le jargon des forestiers, une essence forestière désigne une espèce d'arbre.
            </p>
            <br><br>

            <h4>Duramen</h4>
            <br>
            <p>
            Cœur des troncs d'arbres, partie centrale plus colorée, imputrescible, dépourvue de tissus vivants, souvent dure et lourde.
            </p>
            <br><br>

            <h4>Aubier</h4>
            <br>
            <p>
            Partie tendre et blanchâtre qui se forme chaque année entre le bois dur et l'écorce d'un arbre.
            </p>
            <br><br>
        </article>    
    </section>

    <section>
        <article>
            <h3>Pour aller plus loin</h3>
            <br><br>
            <p>Voici des ressources pour mieux comprendre le bois.
            <br> <br>
            <a class="link" href="https://fr.wikipedia.org/wiki/Essence_foresti%C3%A8re">Essence</a>
            <br> <br>
            <a class="link" href="https://fr.wikipedia.org/wiki/Liste_des_essences_foresti%C3%A8res_europ%C3%A9ennes">Liste des essences</a>

            </p>
        </article>

        <article>
            <h3>Une question ? N'hésitez pas !</h3>
            <br> <br>
            <p> <a class="link" href=contact.ctrl.php>Contactez nous !</a></p>
        </article>
    </section>
    

    <?php include('SubView/footer.subview.php')?>
</body>

</html>