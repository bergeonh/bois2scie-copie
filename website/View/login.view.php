<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/signMaster.css">
    <link rel="stylesheet" href="../Style/login.css">
    <title>Bois 2 Scies - Login</title>
</head>

<body>
    <div class="container-login">
        <h1>Bienvenue</h1>
        <img src="../Resources/bois2scie/SVG/LOGO-BLEU.svg" alt="logo" width="250px">
        <div class="container-form">
            <form class="" action="login.ctrl.php" method="post">
                <div class="group">
                    <output class="w3-pale-red"><?=$erreur?></output>
                   
                    <input type="text" name="email" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label for="mail">Mail</label>
                    
                </div>
    
                <div class="group">
                    
                    <input type="password" name="mdp" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label for="password">Mot de passe</label>
                </div>
                <input type="submit" id='submit' value='Se connecter' class='button'>
            </form>
        </div>
        
            <form action="signup.ctrl.php" method="post">
                <p class="psignbutton">Vous n'avez pas encore de compte ? <br> 
                    <button type="submit" name="action" value="choisir" class="signupbutton" ><a>Créez en un maintenant !</a></button>
                </p>
            </form>
        </div>
    </div>
</body>

</html>