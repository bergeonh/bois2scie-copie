<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/account.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <!-- JS -->
    <script type="text/javascript" src="../Script/account.js" defer></script>
    <script type="text/javascript" src="../Script/buttonToggle.js" defer></script>
    <title>Document</title>
</head>
<body>
            <?php include('SubView/header.subview.php')?>
            <div class="container-compte">
                <h1>Mon compte</h1>
            </div>
            <div class="container-bloc">
                <div class="container-onglets">
                    <ul>
                        <li>
                            <form action="account.ctrl.php" method="get">
                                <button type="submit" name="action" value="Details" class="active" ><i class="fas fa-user"></i> Détails</button>
                            </form>
                        </li>
                        <li>
                            <form action="account.ctrl.php" method="get">
                                <button type="submit" name="action" value="Orders" class="" ><i class="fas fa-shopping-bag"></i> Commandes</button>
                            </form>
                        </li>
                        <li>
                            <form action="account.ctrl.php" method="get">
                                <button type="submit" name="action" value="Params" class="" ><i class="fas fa-cog"></i> Paramètres</button>
                            </form>
                        </li>
                    </ul>
                </div>
                <?php include($sub_view)?>
            </div>
            <?php include('SubView/footer.subview.php')?>
</body>
</html>
