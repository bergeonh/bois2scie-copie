<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/products.css">
    <title>Bois 2 Scies - Produits</title>
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
</head>
<body>
    <?php include('SubView/header.subview.php')?>
    <main>
        <img class="bg_image" src="../Resources/assets/produit_bg_crop.jpg" alt="main">

        <div>
            <h1>Produits</h1>
        </div>
    </main>

    <div class="products-container">

        <?php foreach ($essences as $essence) : ?>
            <div class="product">
                <h3><?= $essence?></h3>
                <a href="../Controller/products.ctrl.php?article=<?= $essence ?>"> <img  src="../Resources/essence/<?= $essence ?>.jpg" alt="<?= $essence ?>"></a>
            </div>
        <?php endforeach; ?>

    </div>

    <?php include('SubView/footer.subview.php')?>
</body>
</html>