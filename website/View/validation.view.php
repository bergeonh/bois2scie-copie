<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/validation.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <script type="text/javascript" src="../Script/panier.js" defer></script>
    <title>Bois 2 Scies - Panier</title>
</head>

<body>

    <?php include('SubView/header.subview.php')?>
    <section>
        <article>
            <i class="fas fa-shopping-basket"></i>
            <h3>Votre Commande a bien été transmise à Bois2Scies</h3>
            <h4>Vous allez être redirigé vers l'accueil dans</h4>
        </article>
    </section>
    <?php include('SubView/footer.subview.php')?>
</body>

</html>