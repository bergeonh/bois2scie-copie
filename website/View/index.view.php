<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="icon" href="../Resources/bois2scie/fav.ico" />
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/index.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <!-- JS -->
    <script type="text/javascript" src="../Script/index.js"></script>
    <script type="text/javascript" src="../Script/index.parallax.js" defer></script>
    <!-- Typed.js -->
    <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>
    

    <title>Bois 2 Scies</title>
</head>

<body>
<?php include('SubView/header.subview.php')?>

    <main>
        <img class="bg_image" src="../Resources/assets/main_bg_crop_static.jpg" alt="main">
        <div>
            <h1>Bois 2 Scies</h1>
            <div class="container">
                <div class="center">
                <form action="products.ctrl.php" method="post">
                <button class="btn" action>
                    <svg width="180px" height="60px" viewBox="0 0 180 60" class="border">
                    <polyline points="179,1 179,59 1,59 1,1 179,1" class="bg-line" />
                    <polyline points="179,1 179,59 1,59 1,1 179,1" class="hl-line" />
                    </svg>
                    <span>Nos essences</span>
                </button>
                </form>
                </div>
            </div>
            <h2><span id="typed"></span></h2>
        </div>
    </main>

    <article>

    <div class="titreIndex">
        <img src="../Resources/bois2scie/PNG/LOGO-BLEU-500px.png" alt="logo" />
    </div>
        

        <h3>Qui sommes-nous ?</h3>
        <div class="firstText">
            <img src="../Resources/assets/vercors_crop.jpg" alt="img1">
             <p>Située aux portes de Grenoble à La Salle-en-Beaumont en Isère sur le plateau Matheysin, bois2scies est une scierie respectueuse de l'environnement. Depuis 2022, bois2scies sublime le bois de forêts provenant des massifs du Vercors, de la Chartreuse et de Belledonne.</p> 
        </div>
        <h3>Que proposons-nous ?</h3>
        <div class="secondText">
            <p>Fiers de proposer une large gamme d'essences de bois, nous mettons à disposition notre expertise au service de vos projets.</p>
            <img src="../Resources/assets/produit2.jpg" alt="img2">
        </div>
        <div id="demos">
            <div>
                <h3>Bardage</h3>
                <img src="../Resources/assets/bardage.jpg" alt="bardage">
            </div>
            <div>
                <h3>Parquet</h3>
                <img src="../Resources/assets/parquet.jpg" alt="parquet">
            </div>
            <div>
                <h3>Terrasse</h3>
                <img src="../Resources/assets/terrasse.jpg" alt="terrasse">
            </div>
        </div>    
    </article>


    <?php include('SubView/footer.subview.php')?>
</body>
</html>