<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/command.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <title>Bois 2 Scies - Récapitulatif</title>
</head>

<body>
<?php include('SubView/header.subview.php')?>

    <main>
        <img src="../Resources/assets/1920x681.png" alt="main">
        <div>
            <h1>Récapitulatif</h1>
        </div>
    </main>


    <!-- TODO -->
    <div class="recap">
        <div class="recap-img">
            <img src="../Resources/assets/594x520.png" alt="img1">
        </div>
        <div class="recap-text">
            <h2>Récapitulatif de la commande : </h2>
            <!-- Boucle PHP -->
            <ul>
                <li>test</li>
                <li>test2</li>
                <li>test3</li>
            </ul>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad sunt voluptatem aut quae doloribus, atque
                saepe minima officia. Officiis earum reprehenderit a at quaerat sit magni alias hic error commodi?</p>
        </div>
        <!-- Validate button -->
        <button>
            <a href="#">Valider</a>
        </button>
    </div>

    <!-- TODO -->


    <?php include('SubView/footer.subview.php')?>
</body>

</html>