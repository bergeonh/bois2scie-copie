<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/about.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <!-- JS -->
    <script type="text/javascript" src="../Script/slideshow.js" defer></script>
    <title>Bois 2 Scies - A propos</title>
</head>
<body>
    <?php include('SubView/header.subview.php')?>

    <main>
        <img class="bg_image" src="../Resources/assets/sechoir_bg.jpg" alt="main">
        <div>
            <h1>A propos</h1>
        </div>
    </main>

    <h2>Notre plus-value : </h2>

    <div class="slideshow-container"><!-- affichage sous forme carrousel -->
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade1">
            <article>    
                <h3>Acheminer le bois au magasin ou à domicile</h3>
                <p>&emsp;Nous livrons autour de l’entreprise dans un rayon de 50km environs, depuis le bassin grenoblois jusqu’au gapençais.<br>
                Pour ses débuts bois 2 scies livre jusqu’à 0,5m3 puis 1,5m3 a venir!<br></p>
            </article>
        </div>

        <div class="mySlides fade1">
            <article>
                <h3>Sécher le bois</h3>
                <p>&emsp;Grâce à notre séchoir passif, nous pouvons mettre à sécher 1,5m3 de bois en accélérant le processus naturel sans déformer le bois. Les délais de séchage varient en fonction de la saison et de l’essence de bois.<br></p>
            </article>
        </div>

        <div class="mySlides fade1">
            <article>
                <h3>Façonner le bois</h3>
                <p>&emsp;En fonction de votre demande, Bois 2 scies réalise une transformation du bois selon les cotes demandées. Différents niveaux de finissions sont possibles.</p>
            </article>
        </div>

        <div class="mySlides fade1">
            <article>
                <h3>Apporter du conseil au client</h3>
                <p>&emsp;Fort de six années d’expériences dans le travail du bois, nous vous proposons de sélectionner vos essences en fonction de leur proximité mais aussi de leurs caractéristiques. Le but étant d’avoir le bois le plus approprié pour vos besoins tout en étant le moins polluant possible.<br></p>
                <a href="../Controller/choose.ctrl.php">Se faire aider</a>
            </article>
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlidesfade1(-1)">&#10094;</a>
        <a class="next" onclick="plusSlidesfade1(1)">&#10095;</a>
        </div>
        <br>

        <!-- The dots/circles -->
        <div style="text-align:center">
        <span class="dot1" onclick="currentSlidefade1(1)"></span>
        <span class="dot1" onclick="currentSlidefade1(2)"></span>
        <span class="dot1" onclick="currentSlidefade1(3)"></span>
        <span class="dot1" onclick="currentSlidefade1(4)"></span>
    </div>



    <div class="slideshow-container">

        <h2>Bois 2 scies améliore les conditions de production et innove !</h2><!-- affichage sous forme carrousel -->
        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade2">
            <article>
                    <h3>Une notation en interne et externe de l’emprunte carbone de ses produits</h3>
                    <p>&emsp;En partenariat avec l’association IMPACT de Grenoble Ecole de Management, Bois 2 scies met en place une notation permettant d’évaluer l’emprunte carbone du bois transformé et vendu. Grâce à cette note, onpeut mesurer la quantité de pollution générée par son activité, améliorer les process de transport et de production et surtout sensibiliser sa clientèle sur le bienfondé de l’achat local!<br></p>
            </article>
        </div>

        <div class="mySlides fade2">
            <article>
                <h3>Un séchoir passif et performant!</h3>
                <p>&emsp;Sur le principe d’une étuve solaire, ce séchoir est chauffé et ventilé par énergie solaire. Ainsi le bois sèche en journée, se repose la nuit, limitant de ce fait les gerces (fissures). Encore en cours d’expérimentation, ce séchoir est un point central de l’entreprise car il garantit à la fois une qualité de séchage au bois mais aussi un impact environnemental mineur grâce à son autonomie énergétique et sa construction issue de produits bois quasi exclusive.<br></p>
            </article>
        </div>

        <div class="mySlides fade2">
        <article>
                <h3>Un système de stockage vertical</h3>
                <p>&emsp; Un système permettant de sélectionner plus facilement les plateaux de bois que l’empilement traditionnel tout un garantissant une compression du bois. Cette idée est simple, elle a pour but de pouvoir prendre n’importe quel plateau de bois sans avoir à faire tomber le plot complet. Le défi de cette réalisation est de conserver entre deux usages une compression suffisante pour maintenir le plateau plan.<br></p>
            </article>
        </div>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlidesfade2(-1)">&#10094;</a>
        <a class="next" onclick="plusSlidesfade2(1)">&#10095;</a>
        </div>
        <br>

        <!-- The dots/circles -->
        <div style="text-align:center">
        <span class="dot2" onclick="currentSlidefade2(1)"></span>
        <span class="dot2" onclick="currentSlidefade2(2)"></span>
        <span class="dot2" onclick="currentSlidefade2(3)"></span>
    </div>


    <?php include('SubView/footer.subview.php')?>
</body>
</html>