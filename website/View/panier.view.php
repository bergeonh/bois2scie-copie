<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/panier.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <script type="text/javascript" src="../Script/panier.js" defer></script>
    <title>Bois 2 Scies - Panier</title>
</head>

<body>

    <?php include('SubView/header.subview.php')?>
    <section>
        <form action="confirmation.ctrl.php" method="post">
            <div class="container-panier">
                <?php if($commande):?>
                <div class="container-item">
                    <!-- boucle php-->
                    <input id="commande" type="hidden" value="<?=$commande->__get('id_commande')?>">
                    <?php $pieces = $commande->__get('pieces'); $sousTotal = 0; ?>
                    <?php foreach($pieces as $piece): ?>
                    <article class="piece">
                        <img src="../Resources/essence/<?=$piece->__get('bois')->getLotBrut()->getEssence()?>.jpg"
                            alt="produit">
                        <div>
                            <h3><?=$piece->getNom()?></h3>
                            <h4 id="prix"><?=round($piece->__get('prix'),2)?> € | <?=$piece->__get('longueur')*$piece->__get('largeur')*$commande->getQuantite($piece)?> m&#xB3; </h4>
                        </div>
                        <div class="quantite">
                        <input id="input-nb" name="nb" onchange="update(this.value,<?=$piece->__get('refPiece')?>)"
                                type="text" value="<?=$commande->getQuantite($piece)?>" required>
                        </div>
                        <h4 id="sousTotal"><?=round($piece->__get('prix'),2) * $commande->getQuantite($piece)?> €</h4>
                        <a class="icon" onclick="del(<?=$piece->__get('refPiece')?>)"><i class="fas fa-trash"></i></a>
                        <?php if(in_array($piece->__get('refPiece'), $erreur)) : ?>
                            <p> Quantité max:  
                            <?= $piece->__get('bois')->getQuantite()?>
                             m&#xB3; pour le lot actuel</p>
                        <?php endif ?>
                    </article>
                    <?php $sousTotal += round($piece->__get('prix'),2) * $commande->getQuantite($piece); $total = $sousTotal*$reduc?>
                    <?php endforeach?>
                </div>

                <div class="container-prix">
                    <h1>Résumé</h1>
                    <div class="resume">
                        <div class="prix">
                            <h2>Sous-total</h2>
                            <h4><?=$sousTotal?> €</h4>
                        </div>
                        <div class="reduc">
                            <?php if(isset($namereduc)) : ?>
                                <input type="text" name="codereduction" id="codereduction" placeholder="Code de Réduction" value="<?=$namereduc?>">
                            <?php else : ?>
                                <input type="text" name="codereduction" id="codereduction" placeholder="Code de Réduction">
                            <?php endif?>
                            <h4 id='submit' onclick="reduction()">Appliquer </h4>
                        </div>
                    </div>
                    <hr /><!-- barre de séparation!-->
                    <div class="prix">
                        <h2>Total</h2>
                        <h4><?= $total?> €</h4>
                    </div>
                    <?php if($erreur != array()) :?>
                        <button class="pay_dis"DISABLED>Valider la commande</button>
                    <?php else : ?>
                        <button class="pay">Valider la commande</button>
                    <?php endif?>
                    
                </div>

                <?php else:?>
                <div id="vide">
                    <img src="../Resources/assets/vide.png">
                    <h3>Votre panier est vide</h3>
                </div>
                <?php endif; ?>
            </div>
        </form>
    </section>
    <?php include('SubView/footer.subview.php')?>
</body>

</html>