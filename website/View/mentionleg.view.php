<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/mentionleg.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">

    <title>Bois 2 Scies - A propos</title>
</head>
<body>
    <?php include('SubView/header.subview.php')?>

    <main>
        <img class="bg_image" src="../Resources/assets/produit_bg_crop.jpg" alt="main">
        <div>
            <h1>Mentions légales</h1>
        </div>
    </main>
    <h2>Dénomination sociale</h2>
    <p>SARL Bois2Scies</p>
    <h2>Siège Social</h2>
    <p>Le plateau Mathézin</p>
    <h2>Contact Dirigeant</h2>
    <p>Tél. : 06 11 22 33 44</br>Mél. : sébastien.pro@bois2scies.fr</p>
    <h2>Capital Social</h2>
    <p>2 000 000 &#8364;</p>
    <h2>Dirigeant</h2>
    <p>Sébastien Pléssala</p>
    <h2>Hébergeur du site</h2>
    <p>Team 5 - UGA, IUT2 dept. informatique (Grenoble)</p>
    <h2>Activité</h2>
    <p>Scierie Locale, découpe, traitement et séchage du bois en Isère (38).</p>
    <p>Numéro chambre des métiers : n°123456789123</p>
    <h2>Mentions sur l'utilisation de données personnelles</h2>
    <p>Sur Bois2Scies, nous ne collections les données personnelles de nos clients (nom, adresse, etc.) qu'avec leur consentement.</br>Ces données nous servent pour constituer des fichiers de clients, qui nous servent par exemple à des campagnes de promotion.</p>



    <?php include('SubView/footer.subview.php')?>
</body>
</html>