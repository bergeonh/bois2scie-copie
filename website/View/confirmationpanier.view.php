<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/panier.css">
    <link rel="stylesheet" href="../Style/confirmationpanier.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <script type="text/javascript" src="../Script/panier.js" defer></script>
    <title>Bois 2 Scies - Panier</title>
</head>

<body>

    <?php include('SubView/header.subview.php')?>
    <section>
    
        <form id="form" action="validation.ctrl.php" method="post">
            <div class="container-panier">
                <?php if($commande):?>
                <div class="container-item">
                    <h2>Vous y êtes presque ! Plus qu'une étape...</h2>
                    <a class="icon" onclick="location.href='panier.ctrl.php';"><i class="fas fa-arrow-circle-left"></i> Retour </a>
                    <!-- boucle php-->
                    <input name = "commande" id="commande" type="hidden" value="<?=$commande->__get('id_commande')?>">
                    <?php $pieces = $commande->__get('pieces'); $sousTotal = 0; ?>
                    <?php foreach($pieces as $piece): ?>
                    <article class="piece">
                        <img src="../Resources/essence/<?=$piece->__get('bois')->getLotBrut()->getEssence()?>.jpg"
                            alt="produit">
                        <div>
                            <h3><?=$piece->getNom()?></h3>
                            <h4 id="prix"><?=$piece->__get('prix')?> €</h4>
                        </div>
                        <div class="quantite">
                            <h4><?=$commande->getQuantite($piece)?></h4>
                        </div>
                        <h4 id="sousTotal"><?=$piece->__get('prix') * $commande->getQuantite($piece)?> €</h4>
                    </article>
                    <?php $sousTotal += $piece->__get('prix') * $commande->getQuantite($piece); $total = $sousTotal*$reduc?>
                    <?php endforeach?>
                </div>

                <div class="container-prix">
                    <h1>Résumé</h1>
                    <div class="resume">
                        <div class="prix">
                            <h2>Sous-total</h2>
                            <h4><?=$total?> €</h4>
                        </div>
                        <div class="prix">
                            <h2>Livraison</h2>
                            <h4><?=$coutLivraison?> €</h4>
                        </div>
                    </div>
                    <hr /><!-- barre de séparation!-->
                    <div class="prix">
                        <h2>Total</h2>
                        <!--la réduction se fait indépendament de la livraison-->
                        <?php $total+=$coutLivraison?>
                        <h4><?= $total?> €</h4>
                        <input type="hidden" name="prix" value="<?=$total?>">
                    </div>
                    <hr /><!-- barre de séparation!-->
                    <div class="infocomplementaires">
                        <h2>Des informations à nous faire parvenir ?</h2>
                        <input name = "info" id="infocomplentaires" type="text" placeholder="Dites nous tout !">
                        <!--On demande a l'utilisateur de fournir un adresse si elle n'a pas été renseigné, puis actualison le prix apres la séléction du bouton modification
                        Il peut également la modifier si elle ne lui convient pas-->
                        <h2>Adresse de livraison : </h2>
                        <input name = "adresse" id="adresse" type="text" <?php if($verif==1){ echo 'value="'.$adresse.'"';} else {echo 'placeholder="'.$adresse.'"';} ?>>
                        <input onclick="EditAdresse()" type="submit" id="upadresse" name="upadresse" value="Modifier"><br>
                        <?php if ($message!=null){echo '<strong>'.$message.'</strong>';}?>
                    </div>
                    <!--bouton de validation accessibel que ci il y'a une adresse valide-->
                    <button class="pay" type="submit" <?php if ($verif==0){echo'disabled';} ?>>Procéder au paiement</button>
                </div>

                <?php else:?>
                <div id="vide">
                    <img src="../Resources/assets/vide.png">
                    <h3>Votre panier est vide</h3>
                </div>
                <?php endif; ?>
            </div>
        </form>
    </section>
    <?php include('SubView/footer.subview.php')?>
</body>

</html>