<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/product.css">
    <link rel="stylesheet" href="../Script/styled-notifications-master/dist/notifications.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <script type="text/javascript" src="../Script/product.js" defer></script>
    <script type="text/javascript" src="../Script/notifs.js" ></script>
    <script src="../Script/styled-notifications-master/dist/notifications.js"></script>
    <title>Bois 2 Scies - Détail produit</title>
</head>

<body>

    <?php include('SubView/header.subview.php')?>

    <div class="product-container">

        <div class="img-container">
            <img  src="../Resources/essence/<?= $_GET['article'] ?>.jpg" alt="product-image-detail">
            <h3 id='art'>Prix au m&#xB3; du <?=$_GET['article']?> <?=$bois->getPrixCube()?> € </h3>
            <?//php if($erreur == 'trop') : ?>
                <h4 id='trop'> Quantité maximum:  <?=$quantite?> m&#xB3; </h4>
            <?//php endif; ?>
        </div>

        <form action="ajout.ctrl.php" method="post">
            <div class="container-item">
                <div class="product-container-right">

                    <h2><?=$_GET['article'] ?></h2>
                    

                    <input id='prix' type="hidden" name='prix' value='<?=$bois->getPrixCube()?>'>
                    <input id='bois' type="hidden" name='bois' value='<?=$bois->getRefBois()?>'>
                    <label for="height">Hauteur (cm)</label>
                    <input id='hauteur' type="number" step="0.001" required="required" name="height" list="height" onchange="update()" placeholder="10">
                    <!-- TODO -->
                    <!-- Lier BDD et option dispo -->
                    

                    <label for="width">Largeur (cm)</label>
                    <input id='largeur' type="number" step="0.001" required="required" name="width" list="width" onchange="update()" placeholder="25">
                    <!-- TODO -->
                    <!-- Lier BDD et option dispo -->


                    <label for="length">Longueur (cm)</label>
                    <input id='longueur' type="number" step="0.001" required="required" name="length" list="length" onchange="update()" placeholder="120">
                    <!-- TODO -->
                    <!-- Lier BDD et option dispo -->


                    <label for="quantity">Quantité</label>
                    <input id='quantite' type="number" required="required" onchange="update()" name="quantity" value="1">

                    <h3 id ='taille'></h3>
                    <h3 id ='total'>-</h3>

                    <input type="submit" class="button" value="Ajouter au panier">
                </div>
            </div>
        </form>
    </div>

    <div class="hr-container">

        <hr>

    </div>


    <div class="description-container">
        <h2>Description globale</h2>
        <p>
            <?= $description ?>
        </p>
    </div>


    <?php if ($_GET['notif'])
            {
                echo 'test';
                    echo '<script> window.onload = ajoutF()</script>';

            }
        ?>

    <?php include('SubView/footer.subview.php')?>


</body>

</html>