<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--CSS link-->
    <link rel="stylesheet" href="../Style/master.css">
    <link rel="stylesheet" href="../Style/contact.css">
    <link rel="stylesheet" href="../Script/styled-notifications-master/dist/notifications.css">
    <link rel="stylesheet" href="../Resources/fontawesome/css/all.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />

    <!--JS Link-->
    <script src="../Script/contact-map.js"></script>
    <script src="../Script/styled-notifications-master/dist/notifications.js"></script>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
   
    <title>Bois 2 Scies - Contact</title>
</head>
<!--On rajoute le header-->
<body>
    <?php include('SubView/header.subview.php')?>

    <main>
        <img src="../Resources/assets/plateau_matheysin.jpg" alt="main">
        <div>
            <h1>Contact</h1>
        </div>
    </main>

    <div class="contact-container">
        <div><!--On affiche les information sur l'entreprise-->
            <section class="info">
                <h2>La scierie</h2> 
                <p>ville/lieu dis<br>
                    adresse<br>
                    france<br>
                </p>
                <p>
                <strong>
                    téléphone : 00 11 22 33 44 <br>
                    adresse@mail.fr
                </strong>
                </p>
            </section>
        </div>
        <!--On affiche la carte avec un pin sur l'adresse de l'entreprsie-->
        <div id="map"></div>  
    </div>
    <div class="container">
        <!-- Ensemble des informations à transmetre pour pouvoir envoyer un mail de contact-->
        <!-- pour le nom/prenom/email, vérifie si il y'a un utilisateur connécté, et si c'est le cas , pré-rempli les champs de texte-->
        <!-- Un email ne peut etre envoyé que si tous le critères n'ont pas été complété-->
        <form action="contact.ctrl.php" method="post">
          <h2>Nous contacter</h2>
          <label for="prenom">Votre Prénom<span class="obligatoire"> * </span></label>
          <input type="text" id="prenom" name="prenom" <?php if(isset($_SESSION['email_utilisateur'])){ echo 'value="'.$prenom.'"';} else {echo 'placeholder="'.$prenom.'"';} ?> required>
      
          <label for="nom">Votre Nom<span class="obligatoire"> * </span></label>
          <input type="text" id="nom" name="nom" <?php if(isset($_SESSION['email_utilisateur'])){ echo 'value="'.$nom.'"';} else {echo 'placeholder="'.$nom.'"';} ?> required>
      
          <label for="email">Votre E-mail<span class="obligatoire"> * </span></label>
          <input type="email" id="email" name="email" <?php if(isset($_SESSION['email_utilisateur'])){ echo 'value="'.$mail.'"';} else {echo 'placeholder="'.$mail.'"';} ?> required>
          
          <label for="title">Titre du message<span class="obligatoire"> * </span></label>
          <input type="text" id="title" name="title" placeholder="Titre du message ..." required>
      
          <label for="subject">Message<span class="obligatoire"> * </span></label>
          <textarea id="subject" name="subject" placeholder="Votre message ..." style="height:200px" required></textarea>

          <input type="submit" name="submit" value="Envoyer" id="submit"> 
          <figcaption class="etoile"> <span class="obligatoire"> * </span> tous les champs sont obligatoires </figcaption>
        </form>
      </div> 
      <!-- vérifie si un mail a été envoyé-->
      <?php if ($test == true)
            {
                // si le mail a été envoyé correctement, affiche une notification de confirmation
                //sinon affiche une notification d'erreur
                
                if ($code==1){
                    echo '<script> window.onload = emailt()</script>';
                } else{
                    echo '<script> window.onload = emailf()</script>';
                }
            }
        ?>
    <footer>
    <!--On rajoute le footer-->
    <?php include('SubView/footer.subview.php')?>
    <!-- droit d'utilisation de la carte-->
        <p><a href="https://www.openstreetmap.org/copyright">© OpenStreetMap contributors</a></p>
    </footer>
</body>
</html>