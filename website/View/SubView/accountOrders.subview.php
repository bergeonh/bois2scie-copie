<div class="container-details">
    
    <div class="container-form">
        <h2>Commandes en cours de livraison: </h2>
        <table>
        <thead>
            <tr>
            <th>État</th>
            <th>Date</th>
            <th>Prix</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($commandes as $commande ):  ?>
                <?php if($commande->__get('etat')=='expedie' || $commande->__get('etat')=='valide') : 
                    $prix=0;
                    foreach ($commande->__get('pieces') as $piece) {
                        $prix+=$piece->__get('prix') * $commande->getQuantite($piece); 
                    }
                ?>
                        
                <tr>
                    <td><?= $commande->__get('etat')?></td>
                    <td><?= $commande->__get('date')?></td>
                    <td><?= $prix?></td>
                </tr>
                <?php  endif; ?>
            <?php  endforeach; ?>

        </tbody>
        </table>

        
        <h2>Commandes passées: </h2>
        <table>
        <thead>
            <tr>
            <th>État</th>
            <th>Date</th>
            <th>Prix</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($commandes as $commande ):?>
                <?php if($commande->__get('etat')=='arrive' || $commande->__get('etat')=='annule') : 
                    $prix=0;
                    foreach ($commande->__get('pieces') as $piece) {
                        $prix+=$piece->__get('prix') * $commande->getQuantite($piece); 
                    }
                    $commandes['prix']=$prix;
                ?>
                        
                <tr>
                    <td><?= $commande->__get('etat')?></td>
                    <td><?= $commande->__get('date')?></td>
                    <td><?= $prix?></td>
                </tr>
                <?php  endif; ?>
            <?php  endforeach; ?>

        </tbody>
        </table>
    </div>
</div>
