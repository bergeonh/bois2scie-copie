<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/signMaster.css">
    <link rel="stylesheet" href="../Style/signup.css">
    <link rel="stylesheet" href="../Style/signup-suite.css">
    <title>Bois 2 Scies - Sign Up</title>
</head>

<body>
    
    <div class="container-login">
        <h1>Bienvenue</h1>
        <img src="../Resources/bois2scie/SVG/LOGO-BLEU.svg" alt="logo" width="250px">
        <div class="container-form">
            <form class="" action="signup.ctrl.php" method="post">
                <input type="hidden" name="page" value="2">
                <div class="group">
                    <input type="text" name="nom" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Nom</label>
                </div>

                <div class="group">
                    <input type="text" name="prenom" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Prénom</label>
                </div>
                <div class="container-button">
                    <button type="submit" name="action" value="finaliser" class="button signup" >Finaliser le compte</button>      
                </div>
                <div class="container-button">
                    <button onclick="location.href='signup.ctrl.php';" class="button cancel" >Retour</button>     
                </div>
               
            </form>
        </div>

        <form action="login.ctrl.php" method="post">
            <p class="psignbutton">Vous avez déjà un compte ? <br>
                <button type="submit" name="action" value="choisir" class="signupbutton"><a>Connectez vous maintenant!</a></button>
            </p>
        </form>
        
    </div>
    
    </div>
</body>

</html>