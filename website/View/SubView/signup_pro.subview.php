<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="../Style/signMaster.css">
    <link rel="stylesheet" href="../Style/signup.css">
    <link rel="stylesheet" href="../Style/signup-suite.css">
    <!-- JS -->
    <script src="../Script/cleave.js"></script>
    <script src="../Script/cleave-fr.js"></script>
    <script src="../Script/signup-pro.js" defer></script>
    <title>Bois 2 Scies - Sign Up</title>
</head>

<body>
    <div class="container-login">
        <h1>Bienvenue</h1>
        <img src="../Resources/bois2scie/SVG/LOGO-BLEU.svg" alt="logo" width="250px">
        <form action="signup.ctrl.php" method="post">
        <div class="container-form">
            <div class="userfillform"> 
                <div class="group">
                    <input id="nom" type="text" name="nom" value="<?= $nom ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Nom</label>
                </div>

                <div  name="prenom" class="group">
                    <input id="prenom" type="text" name="prenom" value="<?= $prenom ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Prenom</label>
                </div>

                <div  name="prenom" class="group">
                    <input id="contact" type="text" name="contact" value="<?= $contact ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Nom contact Entreprise</label>
                </div>

                <div class="group">
                    <input id="telephone" name="telephone" class="input-phone" type="text" value="<?= $telephone ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Numéro de téléphone portable</label>
                </div>

                <div class="group">
                    <input id="telephoneFixe" name="telephoneFixe" class="input-phone" type="text" value="<?= $telephoneFixe ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Numéro de téléphone fixe</label>
                </div>

                <div class="group" >
                    <input type="text" name="siret" value="<?= $siret ?>" id="siret" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Siret</label>
                </div>

                <div class="group">
                    <input id="IBAN" type="text" name="IBAN" value="<?= $IBAN ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>IBAN</label>
                </div>

                <div class="group">
                    <input id="condVente" type="text" name="condVente" value="<?= $condVente ?>" required>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label>Conditions générales de vente</label>
                </div>

            </div>
            <div class="autofillform">
                <div class="group">
                    <input id="nomEntreprise" name="nomEntreprise" value="<?= $nomEntreprise ?>" type="text" disabled>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                </div>

                <div class="group">
                    <input class="input-phone" type="text" value="<?= $tva_intra ?>" disabled>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                </div>

                <div class="group">
                    <input type="text" value="<?= $ape ?>" disabled>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                </div>

                <div class="group">
                    <input class="input-phone" type="text" value="<?= $adresse ?>" disabled>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                </div>
            </div>
        </div>
            
            <div class="container-button">
                <form action="signup.ctrl.php" method="post">
                    <button id="btnSubmit" type="submit" name="action" value="finaliser"          class="button signup" >Finaliser le compte</button>
                </form>
                <button type="button" onclick="location.href='main.ctrl.php';" class="button cancel" >Retour</button>
            </div>
            </form>

            <form action="login.ctrl.php" method="post">
            <p class="psignbutton">Vous avez déjà un compte ? <br>
                <button type="submit" name="action" value="choisir" class="signupbutton"><a>Connectez vous maintenant!</a></button>
            </p>
        </form>
        
    </div>
    
    </div>
</body>

</html>