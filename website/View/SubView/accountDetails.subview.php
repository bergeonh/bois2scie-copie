<div class="container-details">
    <h2>Informations personnelles</h2>
    <div class="container-form">
        <form>
            <div class="group">
                <label for="nom">Nom</label><br>
                <input value="<?=$utilisateur->getNom()?>" type="text" id="nom" name="nom"><br>
            </div>

            <div class="group">
                <label for="prenom">Prénom</label><br>
                <input value="<?=$utilisateur->getPrenom()?>" type="text" id="prenom" name="prenom"><br>
            </div>

            <div class="group">
                <label for="mail">Adresse électronique</label><br>
                <input value="<?=$utilisateur->getEmail()?>" type="text" id="mail" name="mail"><br>
            </div>
            <input type="submit" value="Sauvegarder" class="button">
            <div class="group">
                <label for="tel">Mot de passe</label><br>
                <div id="pass">
                    <input value="<?=$utilisateur->getMdp()?>" type="password" id="pass" name="pass" disabled><br>
                    <form action="passChange.ctrl.php" method="post">
                        <button class="btn" action>Modifier</button>
                    </form>    
                </div>
            </div>
        </form>
    </div>
</div>
