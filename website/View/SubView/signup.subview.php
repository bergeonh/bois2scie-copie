<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/signMaster.css">
    <link rel="stylesheet" href="../Style/signup.css">
    <title>Bois 2 Scies - Sign Up</title>
</head>

<body>
    <div class="container-login">
        <h1>Bienvenue</h1>
        <img src="../Resources/bois2scie/SVG/LOGO-BLEU.svg" alt="logo" width="250px">
        <div class="container-form">
            <form action="signup.ctrl.php" method="post">
                <output class="w3-pale-red"><?=$erreur?></output>
                    <div class="group">
                        <input name="email" type="text" required>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label>Mail</label>
                    </div>

                    <div class="group">
                        <input name="mdp" type="password" required>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label>Mot de passe</label>
                    </div>

                    <div class="group">
                        <input name="mdp_repete" type="password" required>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                        <label>Répéter le mot de passe</label>
                    </div>

                    <div class="container-button">
                        <button type="submit" name="action" value="creer" class="button signup" >Créer un compte</button>
                    </div>
                    
                    <div class="container-button">
                        <button onclick="location.href='login.ctrl.php';" class="button cancel" >Annuler</button>
                    </div>
            </form>
        </div>
        


        <form action="login.ctrl.php" method="post">
            <p class="psignbutton">Vous avez déjà un compte ? <br>
                <button type="submit" name="action" value="choisir" class="signupbutton"><a>Connectez vous maintenant!</a></button>
            </p>
        </form>
        
    </div>
    
    </div>
</body>

</html>