<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/signMaster.css">
    <link rel="stylesheet" href="../Style/signup-choix.css">
    <title>Bois 2 Scies - Sign Up</title>
</head>

<body>
    <div class="container-login">
        <img src="../Resources/bois2scie/SVG/LOGO-BLEU.svg" alt="logo" width="250px">

        <form class="form-choix" action="signup.ctrl.php" method="post">
            <div id="particulier">
                <h2><i class="fas fa-user"></i> Compte particulier</h2>
                <span><i class="fas fa-check"></i> Vous n'êtes pas une entreprise </span>
                <span><i class="fas fa-check"></i> Vous souhaitez acheter du bois pour une utilisation personnelle</span>
                <button name="status" type="submit" value="particulier"   class="button signup">S'inscrire</button>
            </div>
            <div id="professionnel">
                <h2><i class="fas fa-user-tie"></i> Compte professionnel</h2>
                <span><i class="fas fa-check"></i> Vous êtes une entreprise</span>
                <span><i class="fas fa-check"></i> Vous souhaitez acheter du bois pour vos projets professionnels</span>
                <button name="status" type="submit" value="pro" class="button signup">S'inscrire</button>
            </div>

        </form>
        <button type="button" class="button cancel" onclick="location.href='login.ctrl.php';">Annuler</button>
        <form action="login.ctrl.php" method="post">
            <p class="psignbutton">Vous avez déjà un compte ? <br>
                <button type="submit" name="action" value="choisir" class="signupbutton"><a>Connectez vous maintenant!</a></button>
            </p>
        </form>

    </div>

    </div>
</body>

</html>