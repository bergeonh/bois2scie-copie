<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Style/signMaster.css">
    <link rel="stylesheet" href="../Style/signup-choix.css">
    <link rel="stylesheet" href="../Style/confirmation.css">
    <title>Bois 2 Scies - Sign Up</title>
</head>

<body>
    <div class="container-login">
        <h1>Bravo !</h1>
        <img src="../Resources/bois2scie/PNG/LOGO-BLEU-100px.png" alt="logo">

        <p id="text-confirmation">Votre commande a été enregistrée !</p>
        <form class="form-choix" action="signup.ctrl.php" method="post">
            <div id="consulter">
                <button name="status" class="button consulter" onclick="location.href='account.ctrl.php';">Consulter mes informations</button>
            </div>
            <div id="retour">
                <button name="status" class="button retour" onclick="location.href='main.ctrl.php';">Retour à la page principale</button>
            </div>
            
        </form>
    </div>
    
    </div>
</body>

</html>