
<header>
    <nav class="menu-container">
        <!-- burger menu -->
        <input type="checkbox" aria-label="Toggle menu" />
        <span></span>
        <span></span>
        <span></span>
        <!-- logo -->
        <a class="a_header" href="main.ctrl.php" class="menu-logo"><img class='img-logo' src="../Resources/bois2scie/PNG/LOGO-BLANC-100px.png" alt="logo" /></a>
        <!-- menu items -->
        <div class="menu">
            <ul>
                <li><a class="a_header" href="main.ctrl.php"><i class="fas fa-home"></i> Accueil</a></li>
                <li><a class="a_header" href="products.ctrl.php"><i class="fas fa-layer-group"></i> Produits</a></li>
                <li><a class="a_header" href="about.ctrl.php"><i class="fas fa-address-card"></i> À propos</a></li>
                <li><a class="a_header" href="choose.ctrl.php"><i class="fas fa-question-circle"></i> Choisir son bois</a></li>
            </ul>
            <ul>
                <?php if(isset($_SESSION['email_utilisateur'])): ?>
                    <li><a class="a_header" href="panier.ctrl.php"><i class="fas fa-shopping-cart"></i></a></li>
                    <div class="dropdown">
                    <li><a class="a_header" href="account.ctrl.php"><i class="fas fa-user"></i> Mon compte</a></li>
                    <div class="dropdown-content">
                        <a class="a_header" href="logout.ctrl.php"><i class="fas fa-sign-out-alt"></i> Se déconnecter</a>
                    </div>
                </div>
                <?php else: ?> 
                    <li><a class="a_header" href="account.ctrl.php"><i class="fas fa-user"></i> Se connecter / S'inscrire</a></li>   
                <?php endif; ?>

            </ul>
        </div>
    </nav>

</header>
<script src="../Script/header.js"></script>

