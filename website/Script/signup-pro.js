
function phone() {
    var cleave = new Cleave('.input-phone', {
        phone: true,
        phoneRegionCode: 'fr'
    });
}

window.onload = function() {
    phone();
    var siret = document.getElementById("siret");
    siret.onchange = function updateFormulaire() {
        var siret = document.getElementById("siret");
        var btnSubmit = document.getElementById("btnSubmit");
        siret.value = siret.value.replace(/ /g, '');

        if (!(siret.value.length < 14) && !(siret.value.length > 14))
        {
            var iban          = document.getElementById("IBAN");
            var telephone     = document.getElementById("telephone");
            var telephoneFixe = document.getElementById("telephoneFixe");
            var condVente     = document.getElementById("condVente");
            var nom           = document.getElementById("nom");
            var prenom        = document.getElementById("prenom");
            var contact       = document.getElementById("contact");
    
    
            prenom.removeAttribute("required");
            telephone.removeAttribute("required");
            nom.removeAttribute("required");
            telephoneFixe.removeAttribute("required");
            condVente.removeAttribute("required");
            iban.removeAttribute("required");
            contact.removeAttribute("required");
    
            btnSubmit.setAttribute("name","action");
            btnSubmit.setAttribute("value","autocompletion");
            btnSubmit.click();
        }
    }
};


