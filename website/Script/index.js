function typed() {
  var typed = new Typed('#typed', {
    strings: ["Travail du bois", "Eco-responsable", "Local"],
    typeSpeed: 100,
    backSpeed: 50,
    loop: true,
    showCursor: true,
  })
}
