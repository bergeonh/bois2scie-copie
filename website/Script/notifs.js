
function ajoutF(){
    console.log("non");
    window.createNotification({
        displayCloseButton: true,
        positionClass: 'nfc-bottom-right',
        showDuration: 100000,
        theme: 'error'
    })({
        message: "Le produit n'a pas été ajouté au panier"
    });
}

function ajoutT(){
    console.log("oui");
    window.createNotification({
        displayCloseButton: true,
        positionClass: 'nfc-bottom-right',
        showDuration: 100000,
        theme: 'success'
    })({
        message: "Le produit a été ajouté au panier"
    });
}