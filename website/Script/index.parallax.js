// On affiche un élément quand il devient visible sur l'écran
window.addEventListener('scroll', function() {
    var titreIndex = document.querySelector('.titreIndex');
    var firstText = document.querySelector('.firstText');
    var secondText = document.querySelector('.secondText');

    // On récupère les coordonnées des éléments
    var positionTitreIndex = titreIndex.getBoundingClientRect();
    var positionFirstText = firstText.getBoundingClientRect();
    var positionSecondText = secondText.getBoundingClientRect();

    // On observe si 'titreIndex' aka le logo est visible
    // Si oui on applique les effets visuels
    // Si non on ne fait rien
    if(positionTitreIndex.top >= 0 && positionTitreIndex.bottom <= window.innerHeight) {
        document.getElementsByClassName('titreIndex')[0].style.opacity = "1";
        document.getElementsByClassName('titreIndex')[0].style.paddingBottom = "1.5em";
        document.getElementsByClassName('titreIndex')[0].style.paddingTop = "2em";
    }
    
    // On observe si 'firstText' aka Qui sommes-nous ? est visible
    // Si oui on applique les effets visuels
    // Si non on ne fait rien
    if(positionFirstText.top >= 0 && positionFirstText.bottom <= window.innerHeight) {
        document.getElementsByClassName('firstText')[0].style.opacity = "1";
        document.getElementsByClassName('firstText')[0].style.paddingBottom = "1.5em";
        document.getElementsByClassName('firstText')[0].style.paddingTop = "2em";
        document.getElementsByClassName('firstText')[0].style.display = "flex";
    }

    // On observe si 'secondText' aka Que proposons-nous ? est visible
    // Si oui on applique les effets visuels
    // Si non on ne fait rien
    if(positionSecondText.top >= 0 && positionSecondText.bottom <= window.innerHeight) {
        document.getElementsByClassName('secondText')[0].style.opacity = "1";
        document.getElementsByClassName('secondText')[0].style.paddingBottom = "1.5em";
        document.getElementsByClassName('secondText')[0].style.paddingTop = "2em";
        document.getElementsByClassName('secondText')[0].style.display = "flex";
    }

});

window.onload = function () {
    headerBtns = document.getElementsByTagName('a_header');

    for (let i = 0; i < headerBtns.length; i++) {
        headerBtns[i].style.color = "var( --main-blue)";
    }

    document.getElementsByClassName('img-logo')[0].src = "../Resources/bois2scie/PNG/LOGO-BLEU-100px.png";

    typed();
}

document.getElementsByTagName('body')[0].onscroll = function() {

    var y = window.scrollY;

    if (y == 0) {
        document.getElementsByClassName('menu-container')[0].style.background = "rgba(255, 255, 255, 0)";
        headerBtns = document.getElementsByClassName('a_header');

        for (let i = 0; i < headerBtns.length; i++) {
            headerBtns[i].style.color = "white";
        }

        document.getElementsByClassName('img-logo')[0].src = "../Resources/bois2scie/PNG/LOGO-BLEU-100px.png";

    } 
    else
    {
        document.getElementsByClassName('menu-container')[0].style.background = "rgba(255, 255, 255, 1)";
        headerBtns = document.getElementsByClassName('a_header');

        for (let i = 0; i < headerBtns.length; i++) {
            headerBtns[i].style.color = "var( --main-blue)";
        }

        document.getElementsByClassName('img-logo')[0].src = "../Resources/bois2scie/PNG/LOGO-BLEU-100px.png";
    }

};