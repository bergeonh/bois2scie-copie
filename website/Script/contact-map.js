// On initialise la latitude et la longitude de Paris (centre de la carte)
var lat = 45.19195; 
var lon = 5.71722;
var zoom = 5;
var macarte = null;
// Fonction d'initialisation de la carte
function initMap() {
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map('map').setView([lat, lon], 15);
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);
    var marker = L.marker([lat, lon]).addTo(macarte);
    macarte.scrollWheelZoom.disable();
}

function emailt(){
    console.log("oui");
    window.createNotification({
        displayCloseButton: true,
        positionClass: 'nfc-top-right',
        showDuration: 100000,
        theme: 'success'
    })({
        message: "Le mail a bien été envoyé"
    });
}

function emailf(){
    console.log("non");
    window.createNotification({
        displayCloseButton: true,
        positionClass: 'nfc-top-right',
        showDuration: 100000,
        theme: 'error'
    })({
        message: "L'envoi du mail a échoué"
    });
}


window.onload = function(){
// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
initMap(); 
};