<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/BoisTransforme.class.php");
require_once("../Model/DAO.class.php");

session_start();
//initialisations
$dao = new DAO();
$view = new View();
$verif = 0; //permet de vérifier si on a une adresse valide
$coutLivraison = 0; // le cout de livraison (0 is pas d'adresse valide)
$utilisateur = $dao->getFromMail($_SESSION['email_utilisateur']); //On récupère l'utilisateur
$message = null;

//paramètres
$PrixAuKm = 2;
$limitation = 25;

//On Vérifie si une nouvelle adresse a été insscrit par l'utilisateur
//Puis On l'enregistre si c'est le cas ou si il y'en a une de base.
//Enfin on met un boolean afin de séléctionner le code dans le cas ou on a une adresse
if (isset($_POST['upadresse'])){
  if (isset($_POST['adresse'])){
    $adresse = $_POST['adresse'];
    $verif=1;
  }else{
    $adresse = "Veuillez insérer votre adresse de livraison";
  }
}else{
  if ($utilisateur->getAdresse()!=null){
    $adresse = $utilisateur->getAdresse();
    $verif=1;
  }else{
    $adresse = "Veuillez insérer votre adresse de livraison";
  }
}

// Si on a une adresse, on 
if ($verif=1){
  str_replace(" ","%20",$adresse);// on remplace les espaces afin que l'url soit valide
  //On récupère les informations sur l'adresse recherché via url
  //On a donc besoin d'un agent http
  //et de récupérer un fichier json depuis l'url

  $urlAdresse = "https://nominatim.openstreetmap.org/search/$adresse?format=json";
  $curlAdresse = curl_init();
  curl_setopt($curlAdresse, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
  curl_setopt($curlAdresse, CURLOPT_URL, $urlAdresse);
  curl_setopt($curlAdresse, CURLOPT_RETURNTRANSFER, 1);
  $reponseAdresse= curl_exec($curlAdresse);
  $fichierAdresse= json_decode($reponseAdresse,true);

  //si le fichier est invalide donc que l'adresse donnée en marche pas
  if($fichierAdresse[0]["lat"]!=null){

    // On récupère les coordonées géographqie grâce au json de l'adresse foruni par le client
    $lat = $fichierAdresse[0]["lat"];
    $lon = $fichierAdresse[0]["lon"];

    //(marche uniquement sur un serveur local setup ==> voir readme)
    //tuto suivit : https://reckoningrisk.com/OSRM-server/
    //On utilise un map téléchargé et on calclul le chemin menant de la société bois2Scies a l'adresse de client
    //On récupère les informations sur le chemin via url
    //On a donc besoin d'un agent http
    //et de récupérer un fichier json depuis l'url

    $urlDistance = "localhost:5000/route/v1/driving/5.7173,45.19198;$lon,$lat?overview=false";
    $curlDistance = curl_init();
    curl_setopt($curlDistance, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
    curl_setopt($curlDistance, CURLOPT_URL, $urlDistance);
    curl_setopt($curlDistance, CURLOPT_RETURNTRANSFER, 1);
    $responseDistance= curl_exec($curlDistance);
    $fichierDistance= json_decode($responseDistance,true);
    //On cherche la distance donné dans le json
    $distance=$fichierDistance["routes"][0]["distance"];
    //on Vérifie si l'adresse est dans le perimetre livrable(en dessous de $limitation km) sinon affiche un message d'erreur
    if (round($distance/1000)<=$limitation){
      // On calcul le cout de livraison grace a la distance convertie en km et multiplié au prix par km
      $coutLivraison = round($distance/1000)*$PrixAuKm;
    }else{
      $verif = 0;
      $message = "Vous ne pouvez pas être livré a cette adresse (plus de $limitation km)";
    }
  }else{
    $verif = 0;
  }
  
}

if ($commandes = $utilisateur->getCommandes()){
  foreach($commandes as $val){
    if($val->__get('etat') == 'panier'){
      $commande = $val;
      if ($commande->__get('pieces')->count() == 0){
        $commande =0;
      }
    }
  }
} else {
  $commande = null;
}
if (isset($_POST['codereduction'])){
  $reduc = $dao->getReduction($_POST['codereduction']);
} else {
  $reduc = 1;
}

if ($view != NULL){
    $view->assign('message',$message);
    $view->assign('commande',$commande);
    $view->assign('adresse',$adresse);
    $view->assign('verif',$verif);
    $view->assign('coutLivraison',$coutLivraison);
    $view->assign('reduc',$reduc);
    $view->display('../View/confirmationpanier.view.php');
}else{
    var_dump($view);
    
}
    
 ?>