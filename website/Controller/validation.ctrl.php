<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/BoisTransforme.class.php");
require_once("../Model/DAO.class.php");

session_start();

$dao = new DAO();
$view = new View();

$utilisateur = $dao->getFromMail($_SESSION['email_utilisateur']);

foreach($utilisateur->getCommandes() as $commande){
    if ($commande->__get('id_commande') == $_POST['commande']){
        $com = $commande;
        foreach($commande->__get('pieces') as $piece){
            $bois = $piece->__get('bois');
            $quantite = $piece->__get('longueur')*$piece->__get('largeur')*$piece->__get('hauteur')*$commande->getQuantite($piece);
            $bois->__set('quantite', $bois->getQuantite() - $quantite);
            $dao->updateBoisTransforme($bois);
            $commande->__set('etat','valide');
            $dao->updateCommande($commande);
        } 
    }
}

$to  = 'lukasloiodic@gmail.com';
$subject = 'Commande n° '.$com->__get('id_commande');
$message = "L'utilisateur ".$_SESSION['email_utilisateur']." vient de valider la commande n°".$com->__get('id_commande')." pour ".$_POST['prix']." € contenant les pièces suivantes : ";
foreach($com->__get('pieces') as $piece){
    $m = "\nPiece ".$piece->__get('refPiece')." d'essence ".$piece->__get('bois')->getLotBrut()->getEssence()."\n\t- longueur : ".$piece->__get('longueur')."m\n\t- largeur : ".$piece->__get('largeur')."m\n\t- hauteur : ".$piece->__get('hauteur')."m\n\t- Nombre de piece :".$com->getQuantite($piece)."\n\t- Quantite restante : ".$piece->__get('bois')->getQuantite()." m3\n";
    $message = $message.$m;
}

if (isset($_POST['info'])){
    $message = $message."\nL'utilisateur tient à vous faire parvenir ce message : ".$_POST['info'];
}
$email = $_SESSION['email_utilisateur'];
$nom = $utilisateur->getNom();
$prenom = $utilisateur->getPrenom();
$headers[] = 'From: '.$nom.' '.$prenom.' <'.$email.'>';

//Envoie
$test=mail($to,$subject, $message, implode("\r\n", $headers));


$view->display("../View/SubView/confirmation.commande.subview.php");


?>