<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/DAO.class.php");

session_start();

//initiatlisation des variables
$dao = new DAO();
$code = 0; //détermine le message a affiche en cas d'envoie de mail
$test = false;//vérifie si le mail a bien été envoyé

if (isset($_REQUEST['submit'])) {
    // email du destinataire du mail (la société)
    $to  = 'celian.cros@etu.univ-grenoble-alpes.fr'; // notez la virgule

    // On récupère le titre
    if (isset($_POST['title'])) {
        $subject = $_POST['title'];
    }

    // On récupère le message que on modifie pour que ca rentre dans les paramètres d'un mail
    if (isset($_POST['subject'])) {
        $message = $_POST['subject'];
        $message = wordwrap($message, 70, "\r\n");
        $message = str_replace("\n.", "\n..", $message);
    }

    // On récupère l'email , le nom et le prenom pour les mettres en en-tête
    //  cela permet au destinataire de pouvoir répondre directement de continuer la discution
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    if (isset($_POST['nom'])) {
        $nom = $_POST['nom'];
    }
    if (isset($_POST['prenom'])) {
        $prenom = $_POST['prenom'];
    }
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    
    $headers[] = 'From: '.$nom.' '.$prenom.' <'.$email.'>';

    //test afin d'éviter les spams possibles garce a la fonction mail() (ajout possible de sécurité)
    //   if ( preg_match( "/[\r\n]/", $ ) || preg_match( "/[\r\n]/", $to ) ) {
    //    header("location : http://www.example.com/mail-error.php");
    //}
    
    // envoi du mail et récupération du massage de réussite
    $test=mail($to, $subject, $message, implode("\r\n", $headers));

    //détermine quel type de notification vas etre envoyé
    if ($test == true){
        $code = 1;
    }else{
        $code = 2;
    }
}
    //On récupère le mail, le nom et le prenom dans le cas ou un utilisateur est connecté
    //sinon on met des placholders
if (isset($_SESSION['email_utilisateur'])) {
    $mail = $_SESSION['email_utilisateur']; 
    $utilisateur = $dao->getFromMail($mail); 
    $nom = $utilisateur->getNom();
    $prenom = $utilisateur->getPrenom();
  } else {
    $mail = "Votre email ...";
    $nom = "Votre nom ...";
    $prenom = "Votre prénom ...";
}

// creation de la vue 
$view = new View();

//on met test a true afin qu'une notifiction s'affiche (marche si un mail a été envoyé)
if ($code >= 1){
    $test = true;
}

$view->assign('mail',$mail);
$view->assign('prenom',$prenom);
$view->assign('nom',$nom);
$view->assign('test',$test);
$view->assign('code',$code);
$view->display('../View/contact.view.php'); 
?>