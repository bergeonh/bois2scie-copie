<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/DAO.class.php");


$dao = new DAO();
$view = new View();

session_start();
session_write_close();

$view->display('../View/index.view.php');
?>