<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/DAO.class.php");


$dao  = new DAO();
$view = new View();

$email = $_POST['email'] ?? false;
$mdp   = $_POST['mdp']   ?? false;


if ($email != false && $mdp != false)                                     // L'email et le mot de passe ont été soumis ?
{                                                                         
  $utilisateur = $dao->getFromMail($email);
  if($utilisateur != NULL)                                                // L'email correspond à un utilisateur ?
  {                                     
    if (password_verify($mdp, $utilisateur->getMdp()))                    // Mot de passe bon ?
    {         
      session_start();
      $_SESSION['email_utilisateur'] = $utilisateur->getEmail();
      session_write_close();

      $view->assign('sub_view', 'confirmation.connexion.subview.php'); 
      $view->assign('confirmation_text', 'Vous êtes bien connecté !');
      $view->display('signup.view.php');
      exit();
    }
    $view->assign('erreur','Erreur : identifiant ou email invalide');
  }
  else 
  {
    $view->assign('erreur','Erreur : identifiant ou email invalide');
  }
}
else
{
  $view->assign('erreur','');
}
$view->display('login.view.php');




?>