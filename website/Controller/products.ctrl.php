<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/BoisTransforme.class.php");
require_once("../Model/DAO.class.php");

session_start();

$dao = new DAO();
$view = new View();
$product = $_GET['article'] ?? false;

if(!$product){//Si on veut accéder au catalogue des essences
    $essences = array();
    $essences = $dao->getEssences();

    $view->assign('essences',$essences);
    $view->display('../View/products.view.php');
}else{//Si on veut accéder au détail d'une essence
    $lot = $dao->getLastLotBrut($product);
    $bois = $dao->getBoisTransformeFromRefLot($lot->getRefLot());

    //Cas ou on a pris trop de bois
    //if (isset($_GET['etat'])){
    //    if ($_GET['etat'] == 'trop'){
            $quantite = $dao->getBoisTransformeMax($product);
            $view->assign('erreur',"trop");
            $view->assign('quantite', $quantite);
    //    }
    //} else {
    //    $view->assign('erreur',"null");
    //}
    $descriptionbois = $dao->getDescriptionEssence($bois->getLotBrut()->getEssence());
    $view->assign('description', $descriptionbois);
    $view->assign('bois',$bois);
    $view->display('../View/product.view.php');
}





    
 ?>