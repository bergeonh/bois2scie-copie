<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/BoisTransforme.class.php");
require_once("../Model/Piece.class.php");
require_once("../Model/DAO.class.php");

session_start();

$asPanier = false;


$dao = new DAO();
$view = new View();

if (!isset($_SESSION['email_utilisateur']))                 // Si l'utilisateur n'est pas connecté
{
    header('Location: login.ctrl.php');
    exit();
} else {
    $bois = $dao->getBoisTransforme($_POST['bois']);
    if ($_POST['height'] * $_POST['width'] * $_POST['length'] * $_POST['quantity'] /1000000 > $bois->getQuantite()){
        $bois = $dao->getBoisTransformeMin($_POST['height'] * $_POST['width'] * $_POST['length'] * $_POST['quantity'], $bois->getLotBrut()->getEssence());
    }
    if ($bois){
        $prix = $bois->getPrixCube() * $_POST['height'] * $_POST['width'] * $_POST['length'] /1000000;

        $piece = new Piece(0,$_POST['length']/100, $_POST['width']/100,$_POST['height']/100, $bois,"type1",$prix);
        //création dynamique
        $utilisateur = $dao->getFromMail($_SESSION['email_utilisateur']);
        foreach($utilisateur->getCommandes() as $com){
            if ($com->__get('etat') == 'panier'){
                $asPanier = true;
                $com->ajouterPiece($piece, $_POST['quantity']);
                $commande = $com;
            }
        }
    
        //Si l'utilisateur n'a pas de panier on lui en créer un 
        if ($asPanier == false){
            //Gestion de la date
            $today = date("j, n, Y");
            $date = explode(', ',$today);
            $year = $date[2][2].$date[2][3];
            $date = $date[0].'/'.$date[1].'/'.$year;
    
            $commande = new Commande(0, $utilisateur, '', '', 'panier', $date); //Pas de depart ni d'arrivee pour le moment
            $commande->ajouterPiece($piece, $_POST['quantity']);
            $utilisateur->addCommande($commande);
        }
    
        //gestion bdd
        $dao->addPiece($piece);
        if ($asPanier == false){
            $dao->addCommande($commande);
        } else{
            
            $dao->addDetail($commande->__get('id_commande'), $piece->__get('refPiece'),  $_POST['quantity']);
        }

        //On renvoie l'utilisateur vers le panier
        $notification = "ajout";
        header('Location: panier.ctrl.php');
        exit();
    } else {
        $bois = $dao->getBoisTransforme($_POST['bois']);
        $article = $bois->getLotBrut()->getEssence();
        header("Location: products.ctrl.php?article=$article&etat=trop&notif=erreur");
        exit();
    }
}



?>
