<?php

session_start();

require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/Entreprise.class.php");
require_once("../Model/BoisTransforme.class.php");
require_once("../Model/DAO.class.php");


$dao  = new DAO();
include("../Controller/signup.ctrl.utils.php");


$view = new View();

$action = $_POST['action'] ?? 'none';
$status = $_POST['status'] ?? 'none';

$view->assign('erreur', ''); 


// Cas 1: Utilisateur déjà connecté
$utilisateur = $_SESSION['email_utilisateur'] ?? false;
if ($utilisateur){       
    header('Location: main.ctrl.php');
    exit();
}


// Cas 2: Statut de l'utilisateur inconnu
if ($action == 'choisir') 
{
    session_destroy();
    $view->assign('sub_view', 'choix.subview.php');             
}


// Cas 3: Statut de l'utilisateur connu
elseif ($action == 'none')
{
    $_SESSION['status'] = $_POST['status'] ?? 'particulier';
    $view->assign('sub_view', 'signup.subview.php');                        
}

                                          
switch ($action) {


    // Cas 4: Soumission du formulaire de création de compte
    case 'creer':
        $view->assign('sub_view', 'signup.subview.php'); 

        $_SESSION['email']  = $_POST['email']       ?? '';
        $_SESSION['mdp']    = $_POST['mdp']         ?? '';
        $mdp_repete         = $_POST['mdp_repete']  ?? '';
        $_SESSION['email']  = filter_var($_SESSION['email'], FILTER_VALIDATE_EMAIL);
    

        if ($_SESSION['mdp'] != $mdp_repete)  //Erreur: mdp pas identiques
        {
            $view->assign('erreur', 'Les mots de passe ne correspondent pas');
        }
        elseif (!$_SESSION['email'] || $dao->getFromMail($_SESSION['email']) != NULL)  //Erreur: mail pas bon          
        {
            $view->assign('erreur', 'Email invalide');    
        }
        else //Validation, on passe à la suite
        {
            switch ($_SESSION['status']) {
                case 'particulier':
                    $view->assign('sub_view', 'signup_particulier.subview.php');
                    break;
                

                // Initialisation des valeurs du formulaire final
                case 'pro':

                    $view->assign('sub_view', 'signup_pro.subview.php');
                    $view->assign('nom', '');
                    $view->assign('prenom', '');
                    $view->assign('contact', '');
                    $view->assign('telephone', '');
                    $view->assign('telephoneFixe', '');
                    $view->assign('siret', '');
                    $view->assign('IBAN', '');
                    $view->assign('condVente', '');
                    $view->assign('nomEntreprise', "Nom de l'entreprise");
                    $view->assign('tva_intra', "N°TVA");
                    $view->assign('ape', 'Code APE');
                    $view->assign('adresse', 'Adresse');
                    break;     
            }
        }
        break;



    // Cas 5: SIRET écris
    case 'autocompletion': 

        $nom           = $_POST['nom']          ?? '';
        $prenom        = $_POST['prenom']       ?? '';
        $contact       = $_POST['contact']      ?? '';
        $telephone     = $_POST['telephone']    ?? '';
        $telephoneFixe = $_POST['telephoneFixe']?? '';
        $siret         = $_POST['siret']        ?? '';
        $IBAN          = $_POST['IBAN']         ?? '';
        $condVente     = $_POST['condVente']    ?? '';

        $entreprise = getEntreprise($siret);


        // Attribution des valeurs du formulaire final
        $view->assign('sub_view'      , 'signup_pro.subview.php'); 
        $view->assign('nom'           , $nom);
        $view->assign('prenom'        , $prenom);
        $view->assign('contact'       , $contact);
        $view->assign('telephone'     , $telephone);
        $view->assign('telephoneFixe' , $telephoneFixe);
        $view->assign('siret'         , $siret);
        $view->assign('IBAN'          , $IBAN);
        $view->assign('condVente'     , $condVente);
        $view->assign('nomEntreprise' , $entreprise['nomEntreprise']);
        $view->assign('tva_intra'     , $entreprise['tva_intra']);
        $view->assign('ape'           , $entreprise['ape']);
        $view->assign('adresse'       , $entreprise['adresse']);
        break;




    // Cas 6: Soumission de la finalisation de la création du compte    
    case 'finaliser':                           
        
        switch ($_SESSION['status']) {
            case 'particulier':

                $nom    = $_POST['nom']       ?? '';
                $prenom = $_POST['prenom']    ?? '';

                // Ajout d'un utilisateur à la base de donnée
                $ajoutUtilisateur = ajouterUtilisateur($dao, $_SESSION['email'], $_SESSION['mdp'], $nom, $prenom, 'particulier', 0);
                $_SESSION['email_utilisateur'] = $_SESSION['email'];

                if ($ajoutUtilisateur == -1)
            
                {
                    $view->assign('erreur', 'Adresse email incorrecte');
                    $view->assign('sub_view', 'signup.subview.php');   
                    break;
                }
                else
                {
                    $view->assign('sub_view', 'confirmation.connexion.subview.php'); 
                    $view->assign('confirmation_text', 'Votre compte a bien été créé et vous êtes connecté !');
                    break;
                }

            
            case 'pro':
                $nom            = $_POST['nom']           ?? false;
                $prenom         = $_POST['prenom']        ?? false;
                $contact        = $_POST['contact']       ?? false;
                $telephone      = $_POST['telephone']     ?? false;
                $telephoneFixe  = $_POST['telephoneFixe'] ?? false;
                $siret          = $_POST['siret']         ?? false;
                $IBAN           = $_POST['IBAN']          ?? false;
                $condVente      = $_POST['condVente']     ?? false;


                // Si une information n'a pas été fournie
                if (!$nom || !$prenom || !$contact || !$telephone || !$telephoneFixe || !$siret || !$IBAN || !$condVente)
                {
                    header('Location: main.ctrl.php');                                     
                    exit();
                    break;   
                }


                // Si le numéro de siret a déjà été enregistré
                if ( $siretDejaEnregistre = NULL)
                {
                    $view->assign('erreur', 'Numéro de siret déjà enregistré');
                    break;
                }

                
                // Récupération des information grâce au siret fournit
                $entreprise = getEntreprise($siret);
                
  


                // Si le numéro de siret fournit n'est pas valide
                if ($entreprise['adresse'] == 'Adresse'){  
                    // True si l'adresse est la valeur par défaut 'Adresse'
                    $view->assign('erreur', 'Mauvais numéro de siret');
                    break;
                }

                // Ajout de l'entreprise à l'utilisateur à la base de donnée
                ajouterEntreprise($dao, $_SESSION['email'], $entreprise, $IBAN, $telephone, $telephoneFixe, $contact, $condVente);
            
                
 

                // Ajout d'un utilisateur à la base de donnée
                ajouterUtilisateur($dao, $_SESSION['email'], $_SESSION['mdp'], $nom, $prenom, 'entreprise', $siret);
                $_SESSION['email_utilisateur'] = $_SESSION['email'];

                $view->assign('sub_view', 'confirmation.connexion.subview.php'); 
                $view->assign('confirmation_text', 'Votre compte a bien été créé et vous êtes connecté !');
                break;  
        }
        break;
}


$view->display('signup.view.php');
?>