<?php
require_once("../Model/Utilisateur.class.php");
require_once("../Model/Entreprise.class.php");
require_once("../Model/DAO.class.php");




function ajouterUtilisateur(DAO $dao, string $email, string $mdp, string $nom, string $prenom, string $status, string $siret): int 
{

    // Hachage du mot de passe
    $mdp = password_hash($_SESSION['mdp'], PASSWORD_ARGON2I, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

    // Ajout de l'utilisateur à la base de donnée
    $ajoutUtilisateur = $dao->addUtilisateur($_SESSION['email'], $mdp);


    if ($ajoutUtilisateur != 0)
    {
        return -1;
    }
    else
    {
        $id_utilisateur = $dao->getFromMail($email)->getIdUtilisateur();
        if ($siret != 0)
        {
            $entreprise = $dao->getEntreprise($dao->getEntrepriseFromSiret($siret));
            $utilisateur = new Utilisateur($id_utilisateur, $email, $mdp, $prenom, $nom, '', $status, $entreprise , date('d/m/y'), date("d/m/y", strtotime("+2 years")));
        }
        else 
        {
            $utilisateur = new Utilisateur($id_utilisateur, $email, $mdp, $prenom, $nom, '', $status, null, date('d/m/y'), date("d/m/y", strtotime("+2 years")));
        }
        $dao->updateUtilisateur($utilisateur);

        return 0;
    }
}

function ajouterEntreprise(DAO $dao, string $email, array $entreprise, string $IBAN, string $telephone, string $telephoneFixe, string $contact, string $condVente): int
{
    $entreprise = new Entreprise(
        0, 
        $entreprise['nomEntreprise'], 
        $contact, 
        $telephoneFixe, 
        $telephone, 
        $entreprise['adresse'], 
        $entreprise['siret'], 
        $entreprise['tva_intra'], 
        $entreprise['ape'], 
        $IBAN,
        $condVente
    );
    return $dao->addEntreprise($entreprise);
}

function getEntreprise(string $siret) : array
{
    $url = "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/$siret";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $response      = curl_exec($curl);
    $entreprise    = json_decode($response,true);
    $nomEntreprise = $entreprise["etablissement"]["unite_legale"]["denomination"]     ?? "Nom de l'entreprise";
    $adresse       = $entreprise["etablissement"]["geo_adresse"]                      ?? "Adresse";
    $tva_intra     = $entreprise["etablissement"]["unite_legale"]["numero_tva_intra"] ?? "N°TVA Intra";
    $ape           = $entreprise["etablissement"]["activite_principale"]              ?? "Code APE";


    if ($tva_intra != "N°TVA Intra")                    // Sinon explode retournerait une chaine vide
    {
        $tva_intra = explode('R', $tva_intra)[1];
    }
    $nomEntreprise = str_replace("'", "", $nomEntreprise);
    

    $entreprise = array(
        'siret'         => $siret,
        'nomEntreprise' => $nomEntreprise,
        'adresse'       => $adresse,
        'tva_intra'     => $tva_intra,
        'ape'           => $ape,
    );

    return $entreprise;
}

?>