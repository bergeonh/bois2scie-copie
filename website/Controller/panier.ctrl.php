<?php
require_once("../Framework/view.class.php");
require_once("../Model/Utilisateur.class.php");
require_once("../Model/BoisTransforme.class.php");
require_once("../Model/DAO.class.php");

session_start();



$dao = new DAO();
$view = new View();
$reduc = 1;

if (!isset($_SESSION['email_utilisateur']))                 // Si l'utilisateur est connecté
{
    header('Location: login.ctrl.php');
    exit();
}
if (isset($_POST['action'])){
  if ($_POST['action'] == 'update'){
    $piece = $dao->getPiece($_POST['piece']);
    $dao->updateDetail($_POST['commande'],$_POST['piece'],$_POST['nb']);
  } else if ($_POST['action'] == 'reduc'){
    $reduc = $dao->getReduction($_POST['codereduction']);
    $view->assign('namereduc',$_POST['codereduction'] );
  }
}
$utilisateur = $dao->getFromMail($_SESSION['email_utilisateur']);

$commande = 0;
if ($commandes = $utilisateur->getCommandes()){
  foreach($commandes as $val){
    if($val->__get('etat') == 'panier'){
      $commande = $val;
      if ($commande->__get('pieces')->count() == 0){
        $dao->removeCommande($commande->__get('id_commande'));
        $commande = 0;
      } else {
        $erreur = array();
        foreach($commande->__get('pieces') as $piece){
          $quantite = $piece->__get('largeur') * $piece->__get('hauteur') * $piece->__get('longueur');
          if ($commande->getQuantite($piece)*$quantite > $piece->__get('bois')->getQuantite()){
            $erreur[] = $piece->__get('refPiece');
          }
        }
        $view->assign('erreur',$erreur);
      }
    }
  }
} else {
  $commande = null;
}


$view->assign('reduc',$reduc);
$view->assign('commande',$commande);
$view->display('../View/panier.view.php');

 ?>
