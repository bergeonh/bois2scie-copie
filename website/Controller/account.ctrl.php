<?php

session_start();

if (!isset($_SESSION['email_utilisateur']))                 // Si l'utilisateur n'est pas connecté
{
    header('Location: login.ctrl.php');                     // On le dirige vers la page de connexion 
    exit();
}

require_once("../Framework/view.class.php");
require_once("../Model/DAO.class.php");


$dao = new DAO();
$view = new View();




//Cas où on est connecté
$utilisateur = $dao->getFromMail($_SESSION['email_utilisateur']); //On récupère l'objet utilisateur à partir de son email stockée en Session
$action = $_GET['action'] ?? 'Details';
if($action=='Orders'){
    $commandes = $dao->getCommandesFromId($utilisateur);
    $view->assign('commandes',$commandes);
    $view->assign('prix',0);
}



$view->assign('utilisateur',$utilisateur); // On envoie l'objet utilisateur dans la vue
$view->assign('sub_view', 'SubView/account'.$action.'.subview.php');
$view->display('../View/account.view.php');
?>