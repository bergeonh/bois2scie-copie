<?php
require_once(__DIR__.'/../Model/Utilisateur.class.php');

echo "Test du constructeur \n";
//tester commandes avant !!

try{
    echo "Création d'un utilisateur vide : ";
    $utilisateur = new Utilisateur();
    echo "OK\n";
}catch(Exception $e){
    exit("\nErreur : ".$e->getMessage()."\n");
}

try{
    echo "Creation d'un Utilisateur avec une entreprise : ";
    $entreprise = new Entreprise(1,"figma","didier","01 02 03 04 05", "06 07 08 09 10", "rue des peupliers", 124487, 115414, "A8E4EHD", "il faut être motivé","ils payent bien !");
    $utilisateur = new Utilisateur(123456789,"james.bond@mail.fr","mabit", "James", "Bond", "12, rue de la paix","entreprise",$entreprise);
    echo "OK\n";
    //test de chaque attribut : 
    $value = $utilisateur->getIdUtilisateur();
    $expected = 123456789;
    if($value != $expected){
        throw new Exception("Erreur sur id '$value', attendu '$expected'");
    }

    $value = $utilisateur->getEmail();
    $expected = "james.bond@mail.fr";
    if($value != $expected){
        throw new Exception("Erreur sur email '$value', attendu '$expected'");
    }

    $value = $utilisateur->getNom();
    $expected = "Bond";
    if($value != $expected){
        throw new Exception("Erreur sur nom '$value', attendu '$expected'");
    }

    $value = $utilisateur->getPrenom();
    $expected = "James";
    if($value != $expected){
        throw new Exception("Erreur sur prenom '$value', attendu '$expected'");
    }

    $value = $utilisateur->getAdresse();
    $expected = "12, rue de la paix";
    if($value != $expected){
        throw new Exception("Erreur sur adresse '$value', attendu '$expected'");
    }

    $value = $utilisateur->getCommandes();
    if(empty($expected)){//on verifie que l'array commande est bien vide
        throw new Exception("Erreur sur commande'$value', attendu vide");
    }

    //test de chaque setteur : 
    $expected = "Valjean";
    $utilisateur->setNom($expected);
    $value = $utilisateur->getNom();
    if($value != $expected){
        throw new Exception("Erreur sur setteur nom '$value', attendu '$expected'");
    }

    $expected = "Jean";
    $utilisateur->setPrenom($expected);
    $value = $utilisateur->getPrenom();
    if($value != $expected){
        throw new Exception("Erreur sur setteur prenom '$value', attendu '$expected'");
    }
    
    $expected = "jean.valjean@mail.fr";
    $utilisateur->setEmail($expected);
    $value = $utilisateur->getEmail();
    if($value != $expected){
        throw new Exception("Erreur sur setteur mail '$value', attendu '$expected'");
    }

    $expected = "22, avenue des champs elysees";
    $utilisateur->setAdresse($expected);
    $value = $utilisateur->getAdresse();
    if($value != $expected){
        throw new Exception("Erreur sur setteur adresse '$value', attendu '$expected'");
    }

    $expected = "entreprise";
    $value = $utilisateur->getStatut();
    if($value != $expected){
        throw new Exception("Erreur sur setteur adresse '$value', attendu '$expected'");
    }

    $expected = "figma";
    $value = $utilisateur->getEntreprise()->__get('nom');
    if($value != $expected){
        throw new Exception("Erreur sur setteur adresse '$value', attendu '$expected'");
    }



    //test autres methodes : 

    $expected = new Commande(1, $utilisateur,'Vizille','grenoble','panier','28/06/2021');
    $utilisateur->addCommande($expected);
    $value = $utilisateur->getCommandes();
    if($value[0] != $expected){
        throw new Exception("Erreur sur methode ajout commande '$value', attendu '$expected'");
    }

    }catch(Exception $e){
        exit("\nErreur : ".$e->getMessage()."\n");
    }


?>