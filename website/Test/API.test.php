<?php

print("On essaye de récupérer le nom de l'entreprise de numéro SIRET: 38456094200045\n");
$url = 'https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/38456094200045';

$curl = curl_init();

// set url
curl_setopt($curl, CURLOPT_URL, $url);
//curl_setopt ($curl, CURLOPT_CAINFO, dirname(__FILE__)."/cacert.pem");
//return the transfer as a string
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// Execute cURL request with all previous settings
$response = curl_exec($curl);
var_dump($json = json_decode($response,true));

if(curl_errno($curl)){
    echo 'Curl error: ' . curl_error($curl);
}else{
    echo 'Résultat de la requête: ' ."\nNom: ". $json["etablissement"]["unite_legale"]["denomination"];
    echo "\nTVA intra: " . $json["etablissement"]["unite_legale"]["numero_tva_intra"];
    echo "\nActivité (APE): " . $json["etablissement"]["activite_principale"];
}
//print_r(curl_getinfo($curl));
// Close cURL session
curl_close($curl);




?>