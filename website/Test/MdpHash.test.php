<?php
require_once(__DIR__.'/../Model/DAO.class.php');
//Test du hachage par algorithme ARGON2I

//1er test avec mot de passe arbitraire
$start = microtime(true);
$hash = password_hash('hahaIlEstSurYahoo', PASSWORD_ARGON2I, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);
echo($hash."\n");
$end = microtime(true);
echo ("\nTemps du hachage: ".$end-$start."\n");
if(password_verify('mdp', $hash)){
    echo ("Mot de passe valide\n");
}

//1er test avec mot de passe d'un utilisateur de la base
echo ("\nTest avec mot de passe de l'utilisateur fictif utilisateur@test.com\n");
$dao = new DAO();
//$user = $dao->getUtilisateur(5);
//if(password_verify('UnMotDePasseTasPeur', $user->getMdp())){
//    echo ("Mot de passe valide\n");
//}else{
//    echo("Mot de passe erronée\n");
//}

?>