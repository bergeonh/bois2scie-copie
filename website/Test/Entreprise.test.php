<?php
require_once(__DIR__.'/../Model/Entreprise.class.php');
  //Test du constructeur
  print("Test du constructeur : ");
  $entreprise = new Entreprise(1,"figma","didier","01 02 03 04 05", "06 07 08 09 10", "rue des peupliers", 124487, 115414, "A8E4EHD", "il faut être motivé","ils payent bien !");
  if ($entreprise == null) {
    throw new Exception("Constructeur ne fonctionne pas");
  }
  print("OK\n");

  
  //Test constructeur par défaut
  print("Test constructeur par défaut : ");
  $entreprisePD = new Entreprise();
  if ($entreprisePD == null) {
    throw new Exception("Constructeur par défaut ne fonctionne pas");
  }
  print("OK\n");


  //Test d'un getter
  print("test getter nom : ");
  $expected = "figma";
  $value = $entreprise->__get('nom');
  if ( $value != $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("getReflot() ne fonctionne pas");
  }
  print("OK\n");

  
  //Test d'un setter
  print("test setter nom : ");
  $entreprise->__set('nom', "figmo");
  $expected = "figmo";
  $value = $entreprise->__get('nom');
  if ( $value != $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("getReflot() ne fonctionne pas");
  }
  print("OK\n");