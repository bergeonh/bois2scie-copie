<?php
require_once(__DIR__.'/../Model/BoisTransforme.class.php');

//Test du constructeur
print("Test du constructeur : ");
$entreprise = new Entreprise(1,"figma","didier","01 02 03 04 05", "06 07 08 09 10", "rue des peupliers", 124487, 115414, "A8E4EHD", "il faut être motivé","ils payent bien !");
$lot = new LotBrut("1",$entreprise, "01/01/01", "Chêne", "héhéha", 9.3, 5.1, "Un bois tah les fous" );
$bois = new BoisTransforme(1, 2.5, 5.1, $lot);
if ($bois == null){
    throw new Exception("Constructeur ne fonctionne pas");
}
print ("OK\n");

//Test de getLotBrut
print("Test de getLotBrut (Avec la ref) : ");
$expected = "1";
$value = $bois->getLotBrut()->getRefLot();
if ($value != $expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("getLotBrut() ne fonctionne pas");
}
print("OK\n");
?>