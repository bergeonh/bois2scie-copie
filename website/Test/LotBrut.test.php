<?php
require_once(__DIR__.'/../Model/LotBrut.class.php');


  //Test du constructeur
  print("Test du constructeur : ");
  $entreprise = new Entreprise(1,"figma","didier","01 02 03 04 05", "06 07 08 09 10", "rue des peupliers", 124487, 115414, "A8E4EHD", "il faut être motivé","ils payent bien !");
  $lot = new LotBrut('A101121Me3SLT', $entreprise, "10/11/21", "Chene", "2/11/21", 9.3, 5.1, "Un bois tah les fous");
  if ($lot == null) {
    throw new Exception("Constructeur ne fonctionne pas");
  }
  print("OK\n");



  //Test de getReflot()
  print("Test de getReflot : ");
  $expected = 'A101121Me3SLT';
  $value = $lot->getReflot();
  if ( $value != $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("getReflot() ne fonctionne pas");
  }
  print("OK\n");


  //Test de setReflot()
  print("Test de setReflot() : ");
  $expected = "A101121Che9fig";
  $lot = new LotBrut('',$entreprise,"10/11/21", "Chene", "2/11/21", 9.3, 5.1, "Un bois tah les fous");
  $lot->setReflot();
  $value = $lot->getReflot();
  if ( $value != $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("setReflot() ne fonctionne pas");
  }
  print("OK\n");
?>
