<?php

require_once(__DIR__.'/../Model/Commande.class.php');
require_once(__DIR__.'/../Model/Piece.class.php');

echo("\nTest de la classe commande\n");
echo("Constructeur: ");
$utilisateur = new Utilisateur(123456789,"james.bond@mail.fr","mabit", "James", "Bond", "12, rue de la paix" );
$commande = new Commande(1,$utilisateur,"10/11/21","13/11/21","expedie","9/11/21");
if ($commande == null) {
    throw new Exception("KO");
  }
echo("OK\n");

echo("Getter: ");
$expected="james.bond@mail.fr";
$value = $commande->__get("utilisateur")->getNom();
if ( $value == $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("setReflot() ne fonctionne pas");
  }
echo(" OK\n");


$entreprise = new Entreprise(1,"figma","didier","01 02 03 04 05", "06 07 08 09 10", "rue des peupliers", 124487, 115414, "A8E4EHD", "il faut être motivé","ils payent bien !");
$lot = new LotBrut("",$entreprise, "10/02/21");
$boisDefaut = new BoisTransforme(1,2.5,3.5,$lot);
$p = new Piece(1,1,$boisDefaut,"unTypeT'asPeur");

$commande->ajouterPiece($p);
echo($commande->getQuantite($p)."\n");
$commande->setQuantite($p,5);
echo($commande->getQuantite($p)."\n");
print("\n \n \n");
print("Prochaine erreur normal : \n");
$commande->setQuantite($p,0);
echo($commande->getQuantite($p)."\n");
?>