<?php
require_once('../Model/DAO.class.php');


try {
  $dao = new DAO(); // Instancie l'objet DAO

  //Test de la récupération d'une entreprise
  print("Accés à une Entreprise : ");
  $expected = new Entreprise(1,"arcopharma","didier","void","void","12ruedeschamps", 110142, 1215412, "jesuisuncodeAPE","void","void", "void" );
  $value = $dao->getEntreprise(1);
  if ( $value != $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture de l'Utilisateur No 1 incorrecte");
  }
  print("OK\n");

  //Test de la récupération d'un Utilisateur
  print("Accès à un Utilisateur : ");
  //$entreprise = new Entreprise(2, "nicolasEntreprise", "aucunContact","00 00 00 00 00","00 00 00 00 00","LePaysDesBisous",110142,1215412,"jesuisunautrecodeAPE","etmoidescoordbankaire", "aucun", "aucunCommentaire");
  $expected = new Utilisateur(2,'sacha.blanco@yahoo.fr','hahaIlEstSurYahoo','Sacha','blanco','ACoteDeLIut',"entreprise",$dao->getEntreprise(2), "00/00/00","00/00/00");
  $value = $dao->getUtilisateur(2);
  if (($value->getEntreprise() != $expected->getEntreprise()) || ($value->getIdUtilisateur() != $expected->getIdUtilisateur())) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture de l'Utilisateur No 1 incorrecte");
  }
  print("OK\n");

  //Test de la récupération d'un LotBrut
  print("Accès à un LotBrut : ");
  $expected = new LotBrut('A101121Mlz15nic',$dao->getEntreprise(2),'10/11/2021','Mélèze','15/11/2021','15.2','950.00','Un bon bois tah lépoque');
  $value = $dao->getLotBrut('A101121Mlz15nic');
  if ( $value != $expected) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture du LotBrut A101121Mé15nic incorrect");
  }
  print("OK\n");


  //Test de la récupération d'un BoisTransforme
  print("Accès à un BoisTransforme : ");
  $lot = $dao->getLotBrut('A101121Mlz15nic');
  $expected = new BoisTransforme(1,163.00,290,$lot);
  $value = $dao->getBoisTransforme(1);
  if ( $value->getRefBois() != $expected->getRefBois()) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture du BoisTransforme A101121Mé15nic incorrect");
  }
  print("OK\n");


  //Test de la récupération d'une Commande
  print("Accès à une Commande : ");
  $utilisateur = new Utilisateur(3,'lukas.loyo@yahoo.fr','mdp','Loyo','lukas','loindeliut', 'entreprise',$dao->getEntreprise(1),"00/00/00","00/00/00");
  $expected = new Commande(5,$utilisateur);
  $value = $dao->getCommande(5);
  if ( $value->__get('id_commande') != $expected->__get('id_commande')) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture de la Commande 2 incorrect");
  }
  print("OK\n");



   //Test de la récupération d'une Pièce
   print("Accès à une Piece : ");
   $lot = $dao->getLotBrut('A101121Mlz15nic');
   $bois = new BoisTransforme(1,10,50,$lot);
   $expected = new Piece(1,1,0.5,0.2,$bois,'type1',12.7);
   $value = $dao->getPiece(1);
   if ( $value->__get('refPiece') != $expected->__get('refPiece')) {
     print("\n");
     var_dump($value);
     print("Attendu : \n");
     var_dump($expected);
     throw new Exception("Lecture de la Piece 1 incorrect");
   }
   print("OK\n");


  
   //Test de la récuperation des commandes d'un utilisateur
   print("Accès au commandes d'un utilisateur : ");
   $utilisateur = $dao->getUtilisateur(3);
   $expected = $dao->getCommande(5);
   $value = $dao->getCommandesFromId($utilisateur);
   if (($value[0]->__get('id_commande') != $expected->__get('id_commande')) && ($value[0]->__get('utilisateur')->getNom() == $expected->__get('utilisateur')->getNom())) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture des commande de l'utilisateur 1 incorrect");
  }
  print("OK\n");



  
  //Test de la récuperation d'un utilisateur + ses commandes
  print("Accés à un Utilisateur et ses commandes : ");
  $expected =  new Utilisateur(1,'hippolyte.bergeon@gmail.com','azerty','Hippolyte','Bergeon','ici');
  for ($i=1; $i < 5 ; $i++) {
    $commande = new Commande($i, $expected);
    $expected->addCommande($commande);
  }
  $value = $dao->getUtilisateur(1);
  if (($value->getIdUtilisateur() != $expected->getIdUtilisateur()) && ($value->getCommandes() == $expected->getCommandes()) ) {
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture de la Piece 1 incorrect");
  }
  print("OK\n");

  //Test de la récupération d'une réduction
  print("Accés à une réduction : ");
  $expected = 0.5;
  $value = $dao->getReduction("demarage");
  if ($value != $expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Lecture de la Reduction lukaslpb incorrect");
  }
  print("OK\n");

  //Test de l'ajout d'un utilisateur s'il n'existe pas déjà
    if($dao->getFromMail('utilisateur@test.com')==NULL){
      print("Ajout d'un utilisateur et test du système de mot de passe : ");
      $expected = 0;
      $hash='$argon2i$v=19$m=2048,t=4,p=3$dUZrNzRkbmpsVTdBUEFDaA$z9UBKUW8kzI3hfWekR0zPHLB+pdS/D8q2L2KxZLeydc';
      $mail='utilisateur@test.com';
      $value = $dao->addUtilisateur($mail,$hash);
      if($value!=$expected){
        print("\n");
        var_dump($value);
        print("Attendu : \n");
        var_dump($expected);
        throw new Exception("Insertion de l'utilisateur non fonctionnelle");
      }
      print("OK\n");
    }else{
      print("L'utilisateur test existe déjà\n");
    }

  
  //Test de la suppression d'un utilisateur s'il existe
  try{
    $user=$dao->getFromMail('utilisateur@test.com');
    print("Supression de l'utilisateur utilisateur@test.com : ");
    $expected = 0;
    $mail='utilisateur@test.com';
    $value = $dao->removeUtilisateur($user->getIdUtilisateur());
    if($value!=$expected){
      print("\n");
      var_dump($value);
      print("Attendu : \n");
      var_dump($expected);
      throw new Exception("Supression de l'utilisateur non fonctionnelle");
    }
    print("OK\n");
  }catch (Exception $e){
    print("L'utilisateur test n'existe pas\n");
  }


  //Test de l'ajout d'une entreprise
  print("Ajout d'une entreprise : ");
  try {
    $expected = 0;
    $entreprise = new Entreprise(0,"Hélikopter Hélikopter", "paraKopter paraKopter","0123456789","9876543210", "UNE POLOGNIE TU ES EFRAYYYYE", 98, 79, "Le code ape il effrayes tes ancetres", "tu ne vas pa me hak", "il faut être beau", "lentreprise de LGBTQ-");
    $value = $dao->addEntreprise($entreprise);
    if($value!=$expected){
      print("\n");
      var_dump($value);
      print("Attendu : \n");
      var_dump($expected);
      throw new Exception("Insertion de l'entreprise non fonctionnelle");
    }
    print("OK\n");
  } catch(Exception $e){
    print("L'entreprise existe déja");
  }

  //Test de la suppresion d'une entreprise si elle existe pas;
  print("Supression d'une entreprise : ");
  $expected = 0;
  $value = $dao->removeEntreprise($entreprise->__get('id_entreprise'));
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  print("OK\n");

  //Test de l'ajout d'un lot
  print("Ajout d'un lot : ");
  try {
    $expected = 0;
    $lot = new LotBrut("temp", $entreprise, "01/01/20", "chêne", "09/11/19",3.2, 5.1, "un bois tah les fous");
    $value = $dao->addLotBrut($lot);
    if($value!=$expected){
      print("\n");
      var_dump($value);
      print("Attendu : \n");
      var_dump($expected);
      throw new Exception("Insertion du lot non fonctionnelle");
    }
    print("OK\n");
  } catch(Exception $e){
    print("Le lot existe déja");
  }

  //Test de la suppression d'un lotBrut si elle existe pas
  print("Supression d'un lot brut : ");
  $expected = 0;
  $value = $dao->removeLotBrut($lot->getRefLot());
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  print("OK\n");


  //Test de l'ajout d'un BoisTransforme
  print("Ajout d'un Bois Transforme : ");
  try {
    $expected = 0;
    $bois = new BoisTransforme("1", 3.5, 7.5, $lot);
    $value = $dao->addBoisTransforme($bois);
    if($value!=$expected){
      print("\n");
      var_dump($value);
      print("Attendu : \n");
      var_dump($expected);
      throw new Exception("Insertion du lot non fonctionnelle");
    }
    print("OK\n");
  } catch(Exception $e){
    print("Le lot existe déja");
  }

  
  //Test de la suppression d'un BoisTransforme
  print("Supression d'un BoisTransformé : ");
  $expected = 0;
  $value = $dao->removeBoisTransforme($bois->getRefBois());
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  print("OK\n");


  //Test de l'ajout d'une piece
  print("Ajout d'une Piece : ");
  try{
    $expected = 0;
    $piece = new Piece(0,3.5,3.5,3.5,$bois,"type1",-1.0);
    $value = $dao->addPiece($piece);
    if($value!=$expected){
      print("\n");
      var_dump($value);
      print("Attendu : \n");
      var_dump($expected);
      throw new Exception("Insertion du lot non fonctionnelle");
    }
    print("OK\n");
  } catch(Exception $e){
    print("Le lot existe déja");
  }

  //Test de la suppression d'une Piece
  print("Supression d'un Piece : ");
  $expected = 0;
  $value = $dao->removePiece($piece->__get('refPiece'));
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  print("OK\n");


  //Test de l'ajout d'une commande
  print("Ajout d'une Commande : ");
  $expected = 0;
  $utilisateur = $dao->getUtilisateur(1);
  $commande = new Commande(5,$utilisateur);
  for ($i = 1; $i < 3; $i++){
    $piece = $dao->getPiece($i);
    $commande->ajouterPiece($piece, 3);
  }
  $value = $dao->addCommande($commande);
  $expected = 0;
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  print("OK\n");
  



  //test de la suppression d'une commande
  print("Suppresion d'une commande : ");
  $expected = 0;
  $value = $dao->removeCommande($commande->__get('id_commande'));
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  print("OK\n");

  //Test de l'ajout d'une reduction
  print("Ajout d'une reduction : ");
  $expected = 0;
  $value = $dao->addReduction('hippolepabo',0.15);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de la reduction non fonctionnelle");
  }
  print("OK\n");

  //Test de la suppression d'une reduction
  $expected = 0;
  $value = $dao->removeReduction('hippolepabo');
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de la reduction non fonctionnelle");
  }
  print("OK\n");



  //Test de la modification d'une entreprise
  print("Modification d'une entreprise : ");
  $expected = 0;
  $entreprise = $dao->getEntreprise(1);
  $nom = $entreprise->__get('nom');
  $entreprise->__set('nom','Diplodocus');
  $value = $dao->updateEntreprise($entreprise);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  $entreprise->__set('nom',$nom);
  $dao->updateEntreprise($entreprise);
  print("OK\n");


  //Test de la modification d'un utilisateur
  print("Modification d'un utilisateur : ");
  $expected = 0;
  $utilisateur = $dao->getUtilisateur(1);
  $nom = $utilisateur->getNom();
  $utilisateur->setNom('hyokopter');
  $value = $dao->updateUtilisateur($utilisateur);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  $utilisateur->setNom($nom);
  $dao->updateUtilisateur($utilisateur);
  print("OK\n");


  //Test de la modification d'un lotBrut
  print("Modification d'un lotBrut : ");
  $expected = 0;
  $lotBrut = $dao->getLotBrut('A101121Mlz15nic');
  $essence = $lotBrut->getEssence();
  $lotBrut->__set('essence','Manganése');
  $value = $dao->updateLotBrut($lotBrut);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  $lotBrut->__set('essence',$essence);
  $dao->updateLotBrut($lotBrut);
  print("OK\n");


  //Test de la modification d'un boisTransforme
  print("Modification d'un Bois Transforme : ");
  $expected = 0;
  $bois = $dao->getBoisTransforme(1);
  $quantite = $bois->getQuantite();
  $bois->__set('quantite',45);
  $value = $dao->updateBoisTransforme($bois);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  $bois->__set('quantite',$quantite);
  $dao->updateBoisTransforme($bois);
  print("OK\n");

  //Test de la modification d'une piece
  print("Modification d'une Piece : ");
  $expected = 0;
  $piece = $dao->getPiece(1);
  $prix = $piece->__get('prix');
  $piece->__set('prix', 98);
  $value = $dao->updatePiece($piece);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  $piece->__set('prix',$prix);
  $dao->updatePiece($piece);
  print("OK\n");

  //Test de la modification d'une commande
  print("Modification d'une commande : ");
  $expected = 0;
  $commande = $dao->getCommande(1);
  $etat = $commande->__get('etat');
  $commande->__set('etat','valide');
  $value = $dao->updateCommande($commande);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Insertion de l'utilisateur non fonctionnelle");
  }
  $commande->__set('etat',$etat);
  $dao->updateCommande($commande);
  print("OK\n");

  //Test de la modification d'une reduction
  print("Modification d'une reduction : ");
  $expected = 0;
  $value = $dao->updateReduction('demarage',0.75);
  if($value!=$expected){
    print("\n");
    var_dump($value);
    print("Attendu : \n");
    var_dump($expected);
    throw new Exception("Modification de la réduction non fonctionnelle");
  }
  $dao->updateReduction('demarage',0.5);
  print("OK\n");

} catch (Exception $e) {
  print("\n*** Erreur ***\n");
  print("Erreur : ".$e->getMessage()."\n");
}


 ?>
