
<?php
require_once(__DIR__.'/Commande.class.php');
require_once(__DIR__.'/BoisTransforme.class.php');
require_once(__DIR__.'/LotBrut.class.php');
require_once(__DIR__.'/Utilisateur.class.php');
require_once(__DIR__.'/Piece.class.php');


// Classe Data Access Object 
class DAO {
  private PDO $db;
  private string $database = 'sqlite:'.__DIR__.'/../Data/bois.db';

  // Constructeur chargé d'ouvrir la BD
  function __construct() {
    try {
      $this->db = new PDO($this->database);
      if (!$this->db) {
        die ("Database error");
      }
    } catch (PDOException $e) {
      die("PDO Error :".$e->getMessage()." $this->database\n");
    }
  }


  // Recupere la ref/l'id du dernier ellement pour la recuperation des autoincrement
  // $table = la table dans lequel on veux chercher
  // Retourne la ref de la table
  function getIdLast(string $table){
    try{
      switch ($table){
        case "BoisTransforme":
          $val = "ref_bois";
          break;
        case "Piece":
          $val = "ref_piece";
          break;
        case "Commande":
          $val = "id_commande";
          break;
        default:
          $val = "e";
          break;  
      }
      $r = $this->db->query("SELECT max($val) FROM $table");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("Table vide\n");
      }
      $val = $table[0][0];
    } catch (PDOException $e) {
      return 1;
    }
    return $val;
  }


  // Recupere les details d'une commandes pour avoir le vecteur de pieces et être accordés avec la base de données
  // $commande = la commande auquel on veut ajouter les pieces
  // retourne une commande avec les Pieces ajoutées
  function getDetails(Commande $commande) : Commande {
    try{
      $id = $commande->__get('id_commande');
      $r = $this->db->query("SELECT * FROM Detail WHERE id_commande = $id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      foreach ($table as $val) {
        $commande->ajouterPiece($this->getPiece($val['ref_piece']), $val['nbPiece']);
      }
    } catch (PDOException $e) {
      return 1;
    }
    return $commande;
  }

  //Acces à une entreprise
  // $id : l'identifiant d'une entreprise
  // Retourne l'Entreprise ayant cet identifiant
  function getEntreprise(int $id) : Entreprise {
    try {
      $r = $this->db->query("SELECT * FROM entreprise WHERE id_entreprise=$id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Entreprise');
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("utilisateur d'id_tilisateur $id non trouvé\n");
      }
      $entreprise = $table[0];
    } catch (PDOException $e) {
      return 1;
    }

    return $entreprise;
  }

  //Acces à l'id d'une entreprise
  // $siret : numero de siret d'une entreprise
  // Retourne l'id ayant ce numerot de siret
  function getEntrepriseFromSiret(int $siret) : int {
    try {
      $r = $this->db->query("SELECT id_entreprise FROM entreprise WHERE siret=$siret");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("entreprise de numero de siret $siret non trouvé\n");
      }
      $id = $table[0][0];
    } catch (PDOException $e) {
      return 1;
    }
    return $id;
  }


  // Acces à un utilisateur
  // $id : l'identifiant d'un utilisateur
  // Retourne l'Utilisateur ayant cet identifiant
  function getUtilisateur(int $id) : Utilisateur {
    // 
    try {
      $r = $this->db->query("SELECT * FROM utilisateur WHERE id_utilisateur=$id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("utilisateur d'id_tilisateur $id non trouvé\n");
      }
      $val = $table[0];
      if ($val['statut'] == "particulier"){
        $utilisateur = new Utilisateur($val['id_utilisateur'],$val['email'],$val['mdp'],$val['prenom'],$val['nom'],$val['adresse'],$val['statut'],null,$val['date_connexion'],$val['date_destruction']);
      } else {
        $utilisateur = new Utilisateur($val['id_utilisateur'],$val['email'],$val['mdp'],$val['prenom'],$val['nom'],$val['adresse'],$val['statut'],$this->getEntreprise($val['id_entreprise']),$val['date_connexion'],$val['date_destruction']);
      }
    } catch (PDOException $e) {
      return 1;
    }
    $commandes = $this->getCommandesFromId($utilisateur);
    if ($commandes != array()){
      foreach ($commandes as $value) {
        $utilisateur->addCommande($value);
      }
    }
    return $utilisateur;
  }

  //acces a un utilisateur d'apres son mail
  //retourne l'utilisateur avec ce nom
  function getFromMail(string $mail) : ?Utilisateur {// "?" car il peut renvoyer un utilisateur null
    try{
      $r = $this->db->query("SELECT id_utilisateur FROM utilisateur WHERE email = '$mail'");
      $table = $r->fetchall();
      //si pas trouvé
      if(count($table)==0){
        return NULL;
      }
      if(count($table)>1){
        throw new Exception("Nombre d'utilisateur incorrect en".__LINE__."\n");
      }

      //trouvé on ne conserve que la seule ligne
      $utilisateur = $this->getUtilisateur($table[0][0]);
      //passage de la table à l'objet
      return $utilisateur;
    } catch(Exception $e){
      exit("\nErreur : ".$e->getMessage()."\n");
  }
  }

  // Acces à un LotBrut
  // $ref : ref_lot d'un LotBrut
  // Retourne le LotBrut ayant cette reference
  function getLotBrut(string $ref) : LotBrut {
    // 
    try {
      $r = $this->db->query("SELECT * FROM LotBrut WHERE ref_lot= '$ref'");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("LotBrut de ref_lot $ref non trouvé\n");
      }
      $val = $table[0];
      $lotBrut = new LotBrut($val['ref_lot'],$this->getEntreprise($val['id_entreprise']),$val['date_achat'],$val['essence'],$val['date_prelevement'],$val['quantite'],$val['prixAchat'],$val['description']);
    } catch (PDOException $e) {
      return 1;
    }
    return $lotBrut;
    //
  }



  // Acces à un BoisTransforme
  // $ref : ref_bois d'un BoisTransforme
  // Retourne le BoisTransforme ayant cette reference
  function getBoisTransforme(int $ref) : BoisTransforme {
    // 
    try {
      $r = $this->db->query("SELECT * FROM BoisTransforme WHERE ref_bois= $ref");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("BoisTransforme de ref_bois $ref non trouvé\n");
      }
      $val = $table[0];
      $bois =  new BoisTransforme($val['ref_bois'], $val['quantite'], $val['prixCube'], $this->getLotBrut($val['ref_lot']));
    } catch (PDOException $e) {
      return 1;
    }
    return $bois;
    //
  }



  // Acces à une Commande
  // $id : id_commande d'une Commande
  // Retourne le Commande ayant cet id
  function getCommande(int $id) : Commande {
    // 
    try {
      $r = $this->db->query("SELECT * FROM Commande WHERE id_commande=$id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("Commande d'id_commande $id non trouvé\n");
      }
      $val = $table[0];
      $commande = new Commande($val['id_commande'], $this->getUtilisateur($val['id_utilisateur']), $val['depart'], $val['arrivee'], $val['etat'], $val['date']);
    } catch (PDOException $e) {
      return 1;
    }

    $commande = $this->getDetails($commande);
    return $commande;
  }


  // Acces à une Piece
  // $ref : ref_piece d'une Piece
  // Retourne la Piece ayant cette reference
  function getPiece(string $ref) : Piece {
    // 
    try {
      $r = $this->db->query("SELECT * FROM Piece WHERE ref_piece=$ref");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        throw new Exception("Piece de ref_piece $ref non trouvée\n");
      }
      $val = $table[0];
      $piece = new Piece($ref, $val['longueur'],$val['largeur'],$val['hauteur'], $this->getBoisTransforme($val['ref_bois']), $val['typePiece'], $val['prix']);
    } catch (PDOException $e) {
      return 1;
    }
    return $piece;
  }


  // Acces a un vecteur de commandes
  // $utilisateur : l'utilisateur en question
  // Retourne un vecteur contenant toute les commandes ayant cette utilisateur
  function getCommandesFromId(Utilisateur $utilisateur) : array {
    try {
      $id = $utilisateur->getIdUtilisateur();
      $r = $this->db->query("SELECT * FROM Commande WHERE id_utilisateur=$id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        return array();
      }
      foreach ($table as $val) {
        $commande = new Commande($val['id_commande'],$utilisateur,$val['depart'], $val['arrivee'], $val['etat'], $val['date']);
        $commandes[] = $this->getDetails($commande); 
      }
    } catch (PDOException $e) {
      return 1;
    }
    return $commandes;
  }


  // Acces a la valeur d'un code de reduction
  // $code : le string contenant le code
  // Retourne une valeur concernant le code en question et 1 sinon
  function getReduction($code){
    try{
      $r = $this->db->query("SELECT value FROM Reduction WHERE nom_reduc = '$code'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      if (count($table) == 0) {
        return 1;
      }
    }catch (PDOException $e) {
        return 1;
    } 
    return $table[0]['value'];   
  }



  // Ajout d'un utilisateur dans la bdd
  // $mail : l'adresse mail utilisée
  // $hash : le mot de passe hashé
  // Retourne 0 si l'utilisateur a été ajouté et 1 sinon (mail déjà utilisé)
  function addUtilisateur(string $mail, string $hash): int {
    $blank="";
    $date = date('d/m/y');
    $dateDestruct = date("d/m/y", strtotime("+2 years"));
    try {
      $r = $this->db->exec(
        "INSERT INTO Utilisateur(email,mdp,prenom,nom,date_connexion,date_destruction)
         VALUES('$mail','$hash','$blank','$blank','$date','$dateDestruct');"
         );
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }

    } catch (PDOException $e) {
      return 1;
    }
    return 0;
  }

  // Ajout d'une entreprise dans la bdd
  // $entreprise = objet entreprise
  // Retourne 0 si l'entreprise a été ajouté et 1 sinon
  function addEntreprise(Entreprise &$entreprise){
    try{
      $nom = $entreprise->__get('nom');
      $nomC = $entreprise->__get('nomContact');
      $telf = $entreprise->__get('tel_fixe');
      $telp = $entreprise->__get('tel_port');
      $adresse = $entreprise->__get('adresse');
      $siret = $entreprise->__get('siret');
      $intra = $entreprise->__get('tva_intra');
      $ape = $entreprise->__get('codeAPE');
      $coord = $entreprise->__get('coord_bank');
      $conditions = $entreprise->__get('conditionGen');
      $commentaire = $entreprise->__get('commentaires');

      $r = $this->db->exec(
        "INSERT INTO Entreprise(nom,nomContact,tel_fixe,tel_port,adresse,tva_intra,siret,codeAPE,coord_bank,conditionGen,commentaires)
         VALUES('$nom','$nomC','$telf','$telp','$adresse','$intra','$siret','$ape','$coord','$conditions','$commentaire');"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $entreprise->__set('id_entreprise',$this->getEntrepriseFromSiret($siret));
      return 0;
    } catch (PDOException $e) {
      return 1;
    }
  }

  

  // Ajout d'un lot brut dans la bdd
  // $lotBrut = objet LotBrut
  // Retourne 1 si l'entreprise a été ajouté et 0 sinon
  function addLotBrut(LotBrut $lotBrut) : int {
    try{
      $lotBrut->setRefLot();
      $ref = $lotBrut->getRefLot();
      $dateA = $lotBrut->getDateAchat();
      $entreprise = $lotBrut->getEntreprise()->__get('id_entreprise');
      $essence = $lotBrut->getEssence();
      $dateP = $lotBrut->getDatePrelevement();
      $quantite = $lotBrut->getQuantite();
      $prixAchat = $lotBrut->getPrixAchat();
      $desc = $lotBrut->getDescription();
      
      $r = $this->db->exec(
        "INSERT INTO LotBrut(ref_lot,id_entreprise,date_achat,essence,date_prelevement,quantite,prixAchat,description)
         VALUES('$ref','$entreprise','$dateA','$essence','$dateP','$quantite','$prixAchat','$desc');"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;
    } catch (PDOException $e) {
      return 1;
    }
  }


  // Ajout d'un Bois transforme dans la bdd
  // $bois = objet BoisTransforme
  // Retourne 1 si le bois a été ajouté et 0 sinon
  function addBoisTransforme(BoisTransforme &$bois) : int {
    try{
      $quantite = $bois->getQuantite();
      $prixCube = $bois->getPrixCube();
      $perte = $bois->getPerte();
      $lot = $bois->getLotBrut()->getRefLot();

      $r = $this->db->exec(
        "INSERT INTO BoisTransforme(ref_lot,prixCube,quantite,perte)
         VALUES('$lot','$prixCube','$quantite','$perte');"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $bois->setRefBois($this->getIdLast("BoisTransforme"));
      return 0;
    } catch (PDOException $e) {
      return 1;
    }
  } 


  // Ajout d'une piece dans la bdd
  // $piece = objet Piece
  // Retourne 1 si la piece a été ajouté et 0 sinon
  function addPiece(Piece &$piece) : int{
    try{
      $longueur = $piece->__get('longueur');
      $largeur = $piece->__get('largeur');
      $hauteur = $piece->__get('hauteur');
      $bois = $piece->__get('bois')->getRefBois();
      $type = $piece->__get('type');
      $prix = $piece->__get('prix');
      $r = $this->db->exec(
        "INSERT INTO Piece(ref_bois,longueur,largeur,hauteur,typePiece,prix)
         VALUES('$bois','$longueur','$largeur','$hauteur','$type','$prix');"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $piece->__set('refPiece',$this->getIdLast('Piece'));
      return 0;
    } catch(PDOException $e){
      var_dump($this->db->errorInfo());
      return 1;
    }
  }

  // Ajout des details dans la base de données
  // $commande = id_commandes
  // $piece = ref_piece 
  // Retourne 1 si la commande a été ajouté et 0 sinon
  function addDetail(int $commande, int $piece, int $nb){
    try{
      $r = $this->db->exec(
        "INSERT INTO Detail(ref_piece,id_commande,nbPiece)
         VALUES('$piece','$commande','$nb');"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;
    } catch(PDOException $e){
      var_dump($this->db->errorInfo());
      return 1;
    }
  }

  // Ajout de la commande et ajout des details
  // $commande = objet Commande
  // Retourne 1 si la commande a été ajouté et 0 sinon 
  function addCommande(Commande &$commande){
    try{
      $utilisateur = $commande->__get('utilisateur')->getIdUtilisateur();
      $depart = $commande->__get('depart');
      $arrive = $commande->__get('arrivee');
      $etat = $commande->__get('etat');
      $date = $commande->__get('date');
      $r = $this->db->exec(
        "INSERT INTO Commande(id_utilisateur,depart,arrivee,etat,date)
         VALUES('$utilisateur','$depart','$arrive','$etat','$date');"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $idCommande = $this->getIdLast('Commande');
      $commande->__set('id_commande',$idCommande);
      foreach ($commande->__get('pieces') as $val => $piece) {
        $quantite = $commande->getQuantite($piece);
        $this->addDetail($idCommande,$piece->__get('refPiece'),$quantite);
      }
      return 0;
    } catch(PDOException $e){
      return 1;
    }
  }

  // Ajout d'une reduction
  // $code = nom de la reduction
  // $val = valeur de la reduction
  // Retourne 1 si la reduction à été ajouté et 0 sinon
  function addReduction($code, $val){
    try{
      $r = $this->db->exec(
        "INSERT INTO Reduction(nom_reduc,value)
        VALUES('$code',$val);"
      );
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;
    }catch(PDOException $e){
      return 1;
    }
  }

  // Supression d'un utilisateur de la bdd
  // $id : l'identifiant de l'utilisateur à supprimer
  // Retourne 0 si l'utilisateur a été supprimé et 1 sinon (l'identifiant n'existe pas)
  function removeUtilisateur(int $id): int {
    try { 
      $r = $this->db->exec("DELETE FROM Utilisateur where id_utilisateur=$id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }

    } catch (PDOException $e) {
      return 1;
    }
    return 0;
  }


  // Supression d'une entreprise dans la bdd
  // $id : l'identifiant de l'entreprise à supprimer
  // Retourne 0 si l'utilisateur à été supprimé et 1 sinon
  function removeEntreprise(int $id): int {
    try{
      $r = $this->db->exec("DELETE FROM Entreprise where id_entreprise = $id");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        throw new Exception("Utilisateur déjà existant");
        return 1;
      }
    } catch (PDOException $e) {
      return 1;
    }
    return 0;
  }



  // Supression d'un lot brut dans la bdd
  // $ref : la référence du lot brut à supprimer 
  // Retourne 0 si le lot à été supprimé et 1 sinon
  function removeLotBrut(string $ref){
    try{
      $r = $this->db->exec("DELETE FROM LotBrut where ref_lot = '$ref'");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        throw new Exception("lotBrut déjà existant");
        return 1;
      }      
    } catch (PDOException $e) {
      return 1;
    }
    return 0;
  }


  // Supression d'un BoisTransforme dans la bdd
  // $ref : la référence du bois à supprimer
  // Retourne 0 si le bois à été supprimé et 1 sinon
  function removeBoisTransforme(string $ref){
    try{
      $r = $this->db->exec("DELETE FROM BoisTransforme WHERE ref_bois = '$ref'");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        throw new Exception("BoisTransforme déjà existant");
        return 1;
      }      
    } catch (PDOException $e) {
      return 1;
    }
    return 0;
  }


  // Supression d'une Piece dans la bdd
  // $ref : la référence d'une piece à supprimer
  // Retourne 0 si le bois à été supprimé et 1 sinon
  function removePiece(string $ref){
    try{
      $r = $this->db->exec("DELETE FROM Piece WHERE ref_piece = '$ref'");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        throw new Exception("Piece déjà existante");
        return 1;
      }      
    } catch (PDOException $e) {
      return 1;
    }
    return 0;
  }

  // Supression des details dans la base de données selon la commande
  // $commande = id_commandes
  // Retourne 0 si la commande a été supprimé et 1 sinon
  function removeDetail(int $commande){
    try{
      $r = $this->db->exec("DELETE FROM Detail WHERE id_commande = '$commande'");
      if ($r == false) {
        return 1;
      }
      return 0;
    } catch(PDOException $e){
      var_dump($this->db->errorInfo());
      return 1;
    }
  }


  // Supression d'un detail dans la base de donné selon 

  // Supression de la commande et des details associés
  // $idCommande = reference de la commande à supprimer
  // Retourne 0 si la commande a été supprimé et 1 sinon 
  function removeCommande(string $id_commande){
    try{
      $this->removeDetail($id_commande);
      $r = $this->db->exec("DELETE FROM Commande WHERE id_commande = '$id_commande'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;
    } catch (PDOException $e) {
      return 1;
    }
  }

  // Supression de la reduction
  // $code = reference de la reduction à supprimer
  // Retourne 0 si la reduction a été suppromer et 1 sinon
  function removeReduction($code){
    try{
      $r = $this->db->exec("DELETE FROM Reduction WHERE nom_reduc = '$code'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;      
    }catch (PDOException $e) {
      return 1;
    }
  }


  // Modification de l'entreprise 
  // $entreprise =  Objet entreprise à modifier
  // Retourne 0 si l'entreprise a été modifier et 0 sinon
  function updateEntreprise(Entreprise $entreprise){
    try{
      $id = $entreprise->__get('id_entreprise');
      $nom = $entreprise->__get('nom');
      $nomContact = $entreprise->__get('nomContact');
      $tel_fixe = $entreprise->__get('tel_fixe');
      $tel_port = $entreprise->__get('tel_port');
      $adresse = $entreprise->__get('adresse');
      $siret = $entreprise->__get('siret');
      $tva_intra = $entreprise->__get('tva_intra');
      $codeAPE = $entreprise->__get('codeAPE');
      $coord_bank = $entreprise->__get('coord_bank');
      $conditionGen = $entreprise->__get('conditionGen');
      $commentaires = $entreprise->__get('commentaires');
      $r = $this->db->exec("UPDATE Entreprise SET nom         = '$nom',
                                                  nomContact  = '$nomContact',
                                                  tel_fixe    = '$tel_fixe',
                                                  tel_port    = '$tel_port',
                                                  adresse     = '$adresse',
                                                  siret       = $siret,
                                                  tva_intra   = $tva_intra,
                                                  codeAPE     = '$codeAPE',
                                                  coord_bank  = '$coord_bank',
                                                  conditionGen = '$conditionGen',
                                                  commentaires = '$commentaires'
                            WHERE id_entreprise = $id");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;                 
    } catch(PDOException $e){
      return 1;
    }
  }


  // Modification de l'utilisateur
  // $Utilisateur = Objet utilisateur à modifier
  // Retourne 0 si l'utilisateur à été modifier et 1 sinon
  function updateUtilisateur(Utilisateur $utilisateur){
    try{
      $id = $utilisateur->getIdUtilisateur();
      $email = $utilisateur->getEmail();
      $mdp = $utilisateur->getMdp();
      $nom = $utilisateur->getNom();
      $prenom = $utilisateur->getPrenom();
      $adresse = $utilisateur->getAdresse();
      $statut = $utilisateur->getStatut();
      if ($statut == 'entreprise'){
        $entreprise = $utilisateur->getEntreprise()->__get('id_entreprise');
      } else {
        $entreprise = 0;
      }
      $date_connexion = $utilisateur->getDateConnexion();
      $date_destruction = $utilisateur->getDateDestruction();
      $r = $this->db->exec("UPDATE UTILISATEUR SET  id_entreprise = '$entreprise',
                                                    email = '$email',
                                                    mdp = '$mdp',
                                                    nom = '$nom',
                                                    prenom = '$prenom',
                                                    adresse = '$adresse',
                                                    statut = '$statut',
                                                    date_connexion = '$date_connexion',
                                                    date_destruction = '$date_destruction'
                            WHERE id_utilisateur = $id");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;  
    }catch(PDOException $e){
      return 1;
    }
  }


  // Modification du Lot Brut
  // $LotBrut = Objet LotBrut à modifier dans la bdd
  // Retourne 0 si le lotbrut a été modifier et 1 sinon
  function updateLotBrut(LotBrut $lotBrut){
    try{
      $refIni = $lotBrut->getRefLot();
      $lotBrut->setRefLot();
      $date_achat = $lotBrut->getDateAchat();
      $entreprise = $lotBrut->getEntreprise()->__get('id_entreprise');
      $essence = $lotBrut->getEssence();
      $date_prelevement = $lotBrut->getDatePrelevement();
      $quantite = $lotBrut->getQuantite();
      $prixAchat = $lotBrut->getPrixAchat();
      $description = $lotBrut->getDescription();
      $refFinal = $lotBrut->getRefLot();
      $r = $this->db->exec("UPDATE LotBrut SET  ref_lot = '$refFinal',
                                                id_entreprise = $entreprise,
                                                date_achat = '$date_achat',
                                                essence = '$essence',
                                                date_prelevement = '$date_prelevement',
                                                quantite = '$quantite',
                                                prixAchat = '$prixAchat',
                                                description = '$description'
                              WHERE ref_lot = '$refIni'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $this->updateLotBois($refIni, $refFinal);
      return 0;  
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }

  // Actualisation de tout les boisTransforme en fonction du nouveau lot
  // $refIni = Référence précédente du lotBrut
  // $refFinal = Nouvel référence du lot
  // Retourne 1 si l'actualisation a eu lieu et 0 sinon
  function updateLotBois($refIni, $refFinal){
    try{
      $r = $this->db->exec("UPDATE BoisTransforme SET ref_lot = '$refFinal'
                          WHERE ref_lot = '$refIni'");
      return 0;
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }
  
  
  // Modification d'un boisTransforme
  // $bois = Objet BoisTransforme à modifier
  // Retourne 0 si l'objet a été transformé et 1 sinon
  function updateBoisTransforme(BoisTransforme $bois){
    try{
      $ref = $bois->getRefBois();
      $quantite = $bois->getQuantite();
      $prixCube = $bois->getPrixCube();
      $perte = $bois->getPerte();
      $lot = $bois->getLotBrut()->getRefLot();
      $r = $this->db->exec("UPDATE BoisTransforme SET ref_lot = '$lot',
                                                      prixCube = $prixCube,
                                                      quantite = $quantite,
                                                      perte = $perte
                            WHERE ref_bois = $ref");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;  
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }

  // Modification d'une piece
  // $piece = Objet Piece à modifier
  // Retourne 0 si l'objet a été transformé et 1 sinon
  function updatePiece(Piece $piece){
    try{
      $ref = $piece->__get('refPiece');
      $longueur = $piece->__get('longueur');
      $largeur = $piece->__get('largeur');
      $hauteur = $piece->__get('hauteur');
      $bois = $piece->__get('bois')->getRefBois();
      $type = $piece->__get('type');
      $prix = $piece->__get('prix');
      $r = $this->db->exec("UPDATE Piece SET  ref_bois = $bois,
                                              longueur = $longueur,
                                              largeur = $largeur,
                                              hauteur = $hauteur,
                                              prix = $prix,
                                              typePiece = '$type'
                            WHERE ref_piece = $ref");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0;  
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }


  // Modification d'une commande 
  // $commande = Objet commande à modifier
  // Retourne 0 si la commande a été transformé et 1 sinon
  function updateCommande(Commande $commande){
    try{
      $id = $commande->__get('id_commande');
      $utilisateur = $commande->__get('utilisateur')->getIdUtilisateur();
      $depart = $commande->__get('depart');
      $arrivee = $commande->__get('arrivee');
      $etat = $commande->__get('etat');
      $date = $commande->__get('date');
      $r = $this->db->exec("UPDATE Commande SET  id_utilisateur  = $utilisateur,
                                              depart = '$depart',
                                              arrivee = '$arrivee',
                                              etat = '$etat',
                                              date = '$date'
                            WHERE id_commande = $id");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0; 
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }

  // Modification d'une reduction
  // $code = Nom de la reduction à modifier
  // $val = nouvelle valeur de la reduction
  // Retourne 0 si la reduction à été modifier et 1 sinon
  function updateReduction($code, $val){
    try{
      $r = $this->db->exec("UPDATE Reduction SET value = $val
                            WHERE nom_reduc = '$code'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      return 0; 
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }


  // FONCTION QUI SONT NON TRIVIAL

  // Recupération de certains attributs de tout les utilisateurs 
  // Retourne tableau d'atribut si la requete a été effectué et 1 sinon 
  function getAdmUtilisateur(){
    try {
      $r = $this->db->query("SELECT email,prenom,nom,adresse,statut FROM Utilisateur");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        return 1;
      }
    } catch (PDOException $e) {
      return 1;
    }
    return $table;
  }


  // Recupération de certains attributs de tout les BoisTransformés
  // Retourne le tableau d'attribut si la requete a été effectué et 1 sinon
  function getAdmBoisTransforme(){
    try{
      $r = $this->db->query("SELECT b.prixCube,b.quantite,l.essence FROM BoisTransforme b
                            INNER JOIN LotBrut l ON l.ref_lot = b.ref_lot 
                            ORDER BY b.quantite");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      // Tests d'erreurs
      if (count($table) == 0) {
        return 1;
      }
    } catch (PDOException $e) {
      return 1;
    }
    return $table;
  }


  // Récupération de certains attributs de toutes les Commandes
  // Retourne le tableau d'attribut si la requete a été effectué et 1 sinon
  function getAdmCommandes(){
    try{
      $r = $this->db->query("SELECT c.id_commande, u.email, c.etat, c.date FROM Commande c
                            INNER JOIN Utilisateur u ON u.id_utilisateur = c.id_utilisateur
                            ORDER BY c.etat");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      $prix = 0;
      foreach ($table as $commandes) {
        $commande = $this->getCommande($commandes['id_commande']);
        foreach ($commande->__get('pieces') as $piece) {
          $prix+=$piece->__get('prix') * $commande->getQuantite($piece); 
        }
        $commandes['prix']=$prix;
        $tableau[]=$commandes;
      }
      //var_dump($table[0]);
      // Tests d'erreurs
      if (count($table) == 0) {
        return 1;
      }
    }catch (PDOException $e) {
      return 1;
    }
    return $tableau;
  }
  
  // Récupération des essences qui ont un boisTransforme
  // Retourne le tableau d'essences si la requete a été effectué et 1 sinon
  function getEssences(){
    try{
      $tableau = array();
      $r = $this->db->query("SELECT DISTINCT essence, ref_lot FROM LotBrut");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      foreach($table as $lot){
        if($this->isTransformed($lot['ref_lot'])){
          if (in_array($lot['essence'], $tableau) == false){
            $tableau[] = $lot['essence'];
          }
        }
      }
      // Tests d'erreurs
      if (count($table) == 0) {
        return 1;
      }
    }catch (PDOException $e) {
      return 1;
    }
    return $tableau;
  }

  // Modification du nombre de piece d'un detail
  // Retourne 0 si le detail à été transformé et 0 sinon
  // supprime le detail et la piece si nb = 0;
  function updateDetail($id_commande, $piece,$nb){
    try{
      $r = $this->db->exec("UPDATE Detail SET nbPiece = $nb
                            WHERE id_commande = $id_commande and ref_piece = $piece");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      if ($nb == 0){
        $r = $this->db->exec("DELETE FROM DETAIL WHERE id_commande = $id_commande and ref_piece = $piece");
        $this->removePiece($piece);
      }
      return 0; 
    }catch(PDOException $e){
      print("Erreur : ".$e->getMessage()."\n");
      return 1;
    }
  }

  // Recuperation du plus vieux lot en fonction d'une essence
  // $lot = lot du bois que l'on veut
  // Retourne l'objet lot le plus vieux
  function getLastLotBrut(string $essence){
    try{
      $r = $this->db->query("SELECT * FROM LotBrut WHERE essence = '$essence'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      foreach($table as $lot){
        if ($this->isTransformed($lot['ref_lot'])){
          $tableau[] = $lot;
        }
      }
      $lot = $this->sortLastLotBrut($tableau);
    }catch (PDOException $e) {
      return 1;
    }
    return $this->getLotBrut($lot);
  }

  //Fonction de tri pour le boisTransforme en fonction de la date
  function sortLastLotBrut($lots){
    $save['ref'] = "null";
    $save['an'] = "999999";
    $save['mois'] = "13";
    $save['jours'] = "32";
    $res = $save;
    foreach($lots as $lot){
      $date = $lot['date_prelevement'];
      $date = explode('/', $date);
      $jours = $date[0];
      $mois = $date[1];
      $an = $date[2];
      if (($an < $save['an']) || ($an == $save['an'] && $mois < $save['mois']) || ($an == $save['an'] && $mois == $save['mois'] && $jours < $save['an'])) {
        $save['ref'] = $lot['ref_lot'];
        $save['an'] = $an;
        $save['mois'] = $mois;
        $save['jours'] = $jours;
      }
    }
    return $save['ref'];
  }

  // Vérifier si un lotBrut a été transformé
  // $ref_lot = reference du lot
  // Renvoie true si lot brut a été transformé et false sinon
  function isTransformed($ref_lot){
    $r = $this->db->query("SELECT ref_bois FROM BoisTransforme WHERE ref_lot = '$ref_lot'");
    if ($r == false) {
      var_dump($this->db->errorInfo());
      return 1;
    }
    $table = $r->fetchAll();
    if (count($table) == 0) {
      return false;
    } else {
      return true;
    }
  }

  //  Recuperer un BoisTransforme en fonction du lotBrut
  //  $lot = refLot
  //  Renvoie un objet BoisTransforme en cas de réussite et 1 sinon
  function getBoisTransformeFromRefLot($lot){
    try{
      $r = $this->db->query("SELECT ref_bois FROM BoisTransforme WHERE ref_lot = '$lot'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      $boisTransforme = $this->getBoisTransforme($table[0]['ref_bois']);
    }catch (PDOException $e) {
      return 1;
    }
    return $boisTransforme;
  }


  // Recuperer le plux vieux BoisTransforme pouvant contenir la quantite passé en parametre
  // $quantite = la quantite minimum
  // Renvoie un objet BoisTransforme en cas de réussite et null sinon
  function getBoisTransformeMin($quantite, $essence){
    try{
      $tableau = null;
      $r = $this->db->query("SELECT * FROM BoisTransforme WHERE quantite >= $quantite and ref_lot IN (SELECT ref_lot FROM LotBrut WHERE essence = '$essence')");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return null;
      }
      $table = $r->fetchAll();
      $i = 0; 
      foreach($table as $bois){
        $lot = $this->getLotBrut($bois['ref_lot']);
        $tableau[$i]['ref_lot'] = $lot->getRefLot();
        $tableau[$i]['date_prelevement'] = $lot->getDatePrelevement();
        $i = $i+1;
      }
      if ($tableau != null){
        $lot = $this->sortLastLotBrut($tableau);
        $bois = $this->getBoisTransformeFromRefLot($lot);
      } else {
        return null;
      }
    }catch (PDOException $e) {
      return null;
    }
    return $bois;
  }


  // Recuperer la quantite max du bois transforme pour une essence donné
  // $essence = essence du bois transforme
  // retourne la quantite max en cas de réussite et null sinon
  function getBoisTransformeMax($essence){
    try{
      $save = 0;
      $r = $this->db->query("SELECT * FROM LotBrut WHERE essence = '$essence'");
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return null;
      }
      $table = $r->fetchAll();
      foreach($table as $lot){
        if ($this->isTransformed($lot['ref_lot'])){
          $bois = $this->getBoisTransformeFromRefLot($lot['ref_lot']);
          if ($bois->getQuantite() > $save){
            $save = $bois->getQuantite();
          } 
        }
      }
    }catch (PDOException $e) {
      return null;
    }
    return $save;
  }


  // Recuperer la description d'une essence
  // $essence = ref d'une essence
  // retourne la description en cas de réussite et null sinon
  function getDescriptionEssence($essence){
    try{
      $r = $this->db->query("SELECT * FROM InfosEssence WHERE essence = '$essence'");
      // Affiche en clair l'erreur PDO si la requête ne peut pas s'exécuter
      if ($r == false) {
        var_dump($this->db->errorInfo());
        return 1;
      }
      $table = $r->fetchAll();
      return $table[0]['description'];
    } catch (PDOException $e) {
      return 1;
    }
    return $commande;
  }
}



?>
