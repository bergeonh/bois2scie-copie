<?php
require_once('LotBrut.class.php');

//classe pour le bois transformé

class BoisTransforme{
    private int $refBois;
    private float $quantite;
    private float $prixCube;
    private float $perte;
    private LotBrut $lot;

    //Constructeur
    function __construct(int $refBois, float $quantite, float $prixCube, LotBrut $lot){
        $this->refBois = $refBois;
        $this->quantite = $quantite;
        $this->prixCube = $prixCube;
        $this->lot = $lot;

        //calcul de la perte
        $this->perte = 10; // pas encore implementé
    }


    //getter
    function getRefBois(){
        return $this->refBois;
    }

    function getQuantite(){
        return $this->quantite;
    }

    function getPrixCube(){
        return $this->prixCube;
    }

    function getPerte(){
        return $this->perte;
    }

    function getLotBrut(){
        return $this->lot;
    }

    //setter
    function setRefBois(int $refBois){
        $this->refBois = $refBois;
    }

    //Setter global
    function __set($name, $value) : void{
        //Attribue à l'attribut de nom $name la valeur $value
        $this->$name = $value;
    }

}

?>