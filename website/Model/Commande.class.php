<?php

require_once('Piece.class.php');
require_once('Utilisateur.class.php');
  

// Description d'une Commande
class Commande {
  private int $id_commande;
  private ?utilisateur $utilisateur;
  private string $depart;
  private string $arrivee;
  private string $etat;
  private string $date;
  private SplObjectStorage $pieces; 

//Constructeur
  function __construct(int $id_commande,?utilisateur $utilisateur,string $depart='',string $arrivee='', string $etat= "panier", string $date='01/01/00') {
    $this->id_commande = $id_commande;
    $this->utilisateur = $utilisateur;
    $this->depart = $depart;
    $this->arrivee = $arrivee;
    $this->etat = $etat;
    $this->date = $date;
    $this->pieces = new SplObjectStorage;
  }


  //getter global
  function __get($name) {
  //Renvoie l'attribut de nom $name
    return $this->$name;
  }

  function __set($name, $value) : void{
    //Attribue à l'attribut de nom $name la valeur $value
    $this->$name = $value;
  }
  
  function ajouterPiece(Piece $p, int $nb = 1){
  //Ajoute un objet pièce à la collection de pièces de la commande, une par défaut
    if($nb>0){
      $this->pieces->attach($p);
      $this->pieces[$p] = $nb;
    }
  }

  function retirerPiece(Piece $p){
    //Retire un objet pièce $p à la collection de pièces de la commande
      $this->pieces->detach($p);
    }

  function setQuantite(Piece $p,int $nb){
    //Ajoute une quantité $nb à la pièce $p de la collection de pièces de la commande
      $this->pieces[$p] = $nb;
      if($nb==0){
        $this->retirerPiece($p);
      }
    }

  function getQuantite(Piece $p) : int{
    //Renvoie la quantité de pièces $p commandée  
    return $this->pieces[$p];
  }


}
?>
