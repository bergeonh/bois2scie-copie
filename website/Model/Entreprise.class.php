<?php
require_once('LotBrut.class.php');

//classe pour les entreprises

class Entreprise{
    private int $id_entreprise;
    private string $nom;
    private string $nomContact;
    private string $tel_fixe;
    private string $tel_port;
    private string $adresse;
    private int $siret;
    private int $tva_intra;
    private string $codeAPE;
    private string $coord_bank;
    private string $conditionGen;
    private string $commentaires;

    //Constructeur
    function __construct(int $id_entreprise = 0, string $nom = '', string $nomContact = '', string $tel_fixe = '', string $tel_port = '', string $adresse = '', int $siret = 0, int $tva_intra = 0, string $codeAPE = '', string $coord_bank = '', string $conditionGen = '', string $commentaires = ''){
        $this->id_entreprise = $id_entreprise;
        $this->nom = $nom;
        $this->nomContact = $nomContact;
        $this->tel_fixe = $tel_fixe;
        $this->tel_port = $tel_port;
        $this->adresse = $adresse;
        $this->siret = $siret;
        $this->tva_intra = $tva_intra;
        $this->codeAPE = $codeAPE;
        $this->coord_bank = $coord_bank;
        $this->conditionGen = $conditionGen;
        $this->commentaires = $commentaires;
    }


    //getter global
    function __get($name) {
        //Renvoie l'attribut de nom $name
        return $this->$name;
    }

    //setter global
    function __set($name, $value) : void{
        //Attribue à l'attribut de nom $name la valeur $value
        $this->$name = $value;
    }

}

?>