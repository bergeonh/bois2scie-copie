<?php
require_once('BoisTransforme.class.php');

//Classe pour représenter les pieces
class Piece{
    private int $refPiece;
    private float $longueur;
    private float $largeur;
    private float $hauteur;
    private BoisTransforme $bois;
    private string $type;
    private float $prix;



    //Constructeur
    function __construct(int $ref_piece, float $longueur,float $largeur,float $hauteur,BoisTransforme $bois , string $type,float $prix = -1.0){
        $this->refPiece = $ref_piece;
        $this->longueur = $longueur;
        $this->largeur = $largeur;
        $this->hauteur = $hauteur;
        $this->bois = $bois;
        $this->type = $type;
        $this->prix = $prix;
    }

    //getter global
  function __get($name) {
  //Renvoie l'attribut de nom $name
    return $this->$name;
  }

  //setter global
  function __set($name, $value) : void{
    //Attribue à l'attribut de nom $name la valeur $value
    $this->$name = $value;
  }

  function getNom() : string{
    return $this->bois->getLotBrut()->getEssence()." - ".($this->longueur*100).'cm /'.($this->largeur*100).'cm /'.($this->hauteur*100).'cm';
  }
}
?>