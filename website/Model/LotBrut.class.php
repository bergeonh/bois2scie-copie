<?php
//Classe pour le bois non travaillé
include_once('Entreprise.class.php');
error_reporting(E_ALL ^ E_DEPRECATED);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
class LotBrut{
    private string $ref_lot;
    private string $date_achat;
    private Entreprise $entreprise;
    private string $essence;
    private string $date_prelevement;
    private float $quantite;
    private float $prixAchat;
    private string $description;

    //Constructeur
    function __construct( string $ref_lot = "", Entreprise $entreprise, string $date_achat, string $essence = "", string $date_prelevement = "", float $quantite = 0, float $prixAchat = 0, string $description = ""){
        $this->date_achat = $date_achat;
        $this->entreprise = $entreprise;
        $this->essence = $essence;
        $this->date_prelevement = $date_prelevement;
        $this->quantite = $quantite;
        $this->prixAchat = $prixAchat;
        $this->description = $description;
        $this->setRefLot();
    }


    //fonctions getter
    function getRefLot(){
        return $this->ref_lot;
    }

    function getDateAchat(){
        return $this->date_achat;
    }

    function getEntreprise(){
        return $this->entreprise;
    }

    function getEssence(){
        return $this->essence;
    }

    function getDatePrelevement(){
        return $this->date_prelevement;
    }

    function getQuantite(){
        return $this->quantite;
    }

    function getPrixAchat(){
        return $this->prixAchat;
    }

    function getDescription(){
        return $this->description;
    }

    //Setter global
    function __set($name, $value) : void{
        //Attribue à l'attribut de nom $name la valeur $value
        $this->$name = $value;
    }



    //fonctions pour avoir un ref_lot , en fonction de la date , de l'essence, d'un volume et d'un fournisseur
    function setRefLot(){
        $date = explode('/', $this->date_achat);
        $an = substr($date[2],-2);
        $date = $date[0].$date[1].$an;

        $abr_essence = $this->getAbreviation($this->essence);

        $quantite = intval($this->quantite);

        $abr_entreprise = $this->getAbreviation($this->entreprise->__get('nom'));
        
        $this->ref_lot = 'A'.$date.$abr_essence.$quantite.$abr_entreprise;
    }


    //fonction pour prendre les 3 premieres lettre non spéciaux d'un mot non spéciales, utile pour les abréviations
    private function getAbreviation(string $text) : string{
        $val = array();
        $nb = 0;
        foreach(str_split($text) as $char){
            if ($nb < 3){
                if(($char >= 'A' && $char <= 'Z') || ($char >= 'a' && $char <= 'z')){
                    $nb ++;
                    $val[] = $char;
                }
            } else {
                break;
            }
        }
        return implode('',$val);
    }
}

?>