<?php
require_once(__DIR__.'/../Model/Commande.class.php');
require_once('Entreprise.class.php');

// Description d'un Utilisateur
class Utilisateur {
  private int $id_utilisateur;
  private string $email;
  private string $mdp;
  private string $nom;
  private string $prenom;
  private string $adresse;
  private string $statut;
  private ?Entreprise $entreprise;
  private string $date_connexion;
  private string $date_destruction;
  private array $commandes;

  function __construct(int $id_utilisateur=0,string $email='',string $mdp='', string $prenom='', string $nom='',string $adresse='',string $statut = 'particulier', ?Entreprise $entrepise = null, string $date_connexion = "", string $date_destruction = "") {
    $this->id_utilisateur = $id_utilisateur;
    $this->email = $email;
    $this->mdp = $mdp;
    $this->nom = $nom;
    $this->prenom = $prenom;
    $this->adresse = $adresse;
    $this->statut = $statut;
    $this->entreprise = $entrepise;
    $this->date_connexion = $date_connexion;
    $this->date_destruction = $date_destruction;
    $this->commandes = array();
  }

  //getters :
  function getIdUtilisateur() : int {
    return $this->id_utilisateur;
  }

  function getEmail() : string{
    return $this->email;
  }

  function getMdp() : string{
    return $this->mdp;
  }

  function getNom() :string{
    return $this->nom;
  }

  function getPrenom() : string{
    return $this->prenom;
  }

  function getAdresse() : string{
    return $this->adresse;
  }

  function getStatut() : string{
    return $this->statut;
  }

  function getEntreprise() : ?Entreprise{
    return $this->entreprise;
  }

  function getDateConnexion() : string{
    return $this->date_connexion;
  }

  function getDateDestruction() : string{
    return $this->date_destruction;
  }
  function getCommandes() : array{
    return $this->commandes;
  }


  //setters :
  //l'utilisateur peut modif uniquement son NOM, PRENOM, EMAIL, ADRESSE

  function setNom(string $nvoNom){
    $this->nom = $nvoNom;
  }

  function setPrenom(string $nvoPrenom){
    $this->prenom = $nvoPrenom;
  }

  function setEmail(string $nvoEmail){
    $this->email = $nvoEmail;
  }

  function setAdresse(string $nvoAdresse){
    $this->adresse = $nvoAdresse;
  }

  function setStatut(string $statut){
    $this->statut = $statut;
  }

  function setEntreprise(Entreprise $entreprise){
    $this->entreprise = $entreprise;
  }

  //autres methodes :
  function addCommande(Commande $cmd){
    array_push($this->commandes, $cmd);
  }

  function getCommande(){
    return $this->commandes;
  }


  //TODO ACTUALISATION DE LA DATE SELON LE RGPD
  function acutaliseDate(string $date){
    print ("TODO");
  }

}
?>